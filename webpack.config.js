const path = require('path');

const forkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const copyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: {
    tab: './src/tab/index.tsx',
    popup: './src/popup/index.tsx',
    background: './src/background/index.ts',
  },
  mode: process.env.NODE_ENV || 'development',
  devtool: process.env.NODE_ENV === 'production' ? 'source-map' : 'cheap-source-map',
  resolve: {
    extensions: [ '.tsx', '.ts', '.js', '.scss', '.sass' ],
    fallback: { crypto: false },
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: 'ts-loader?transpileOnly',
      },
      {
        test: /\.s[ac]ss$/,
        use: [ 'style-loader', 'css-loader', 'sass-loader' ],
      },
    ],
  },
  plugins: [
    new forkTsCheckerWebpackPlugin(),
    new copyWebpackPlugin({
      patterns: [
        { from: 'static', to: '' },
      ],
    }),
  ],
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist'),
  },
};
