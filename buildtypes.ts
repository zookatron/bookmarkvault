import { basename } from 'path';
import { promises as fs } from 'fs';
import { spawn } from 'child_process';
import PromisePool from 'es6-promise-pool';
import ProgressBar from 'progress';

let interval: number;
fs.readdir(`${__dirname}/src/common/types`).then(files => {
  const typeFiles = files.filter(file => file.match(/.ts$/));

  const makePromises = function * () {
    for (const file of typeFiles) {
      yield new Promise((resolve, reject) => {
        const process = spawn(`${__dirname}/node_modules/.bin/typescript-json-schema`, [
          `${__dirname}/src/common/types/${file}`,
          basename(file, '.ts'),
          '--topRef', 'true',
          '--noExtraProps', 'true',
          '--required', 'true',
          '--strictNullChecks', 'true',
          '--out', `${__dirname}/src/common/types/${basename(file, '.ts')}.json`,
        ]);

        let stdout = '';
        let stderr = '';
        process.stdout.on('data', data => { stdout += data; });
        process.stderr.on('data', data => { stderr += data; });
        process.on('error', error => { reject(error); });
        process.on('close', code => { if (code) { reject(new Error(`${stderr}\nExit code: ${code}`)); } else { resolve(stdout); } });
      });
    }
  };

  // tslint:disable-next-line:no-any
  const pool = new PromisePool(makePromises() as any, 8);
  const bar = new ProgressBar('[:bar] :percent :elapsed/:etas', { total: typeFiles.length + 1 });
  bar.tick();
  // tslint:disable-next-line:no-any
  interval = setInterval(() => bar.render(), 100) as any;
  pool.addEventListener('fulfilled', () => bar.tick());

  return pool.start();
}).then(() => console.log('\nType generation complete'), (error: Error) => console.error(`\n${error.message}`)).then(() => clearInterval(interval)); // tslint:disable-line:no-console
