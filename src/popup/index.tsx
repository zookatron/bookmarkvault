import _ from 'lodash';
import React, { useCallback, useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { toJS } from 'mobx';
import classnames from 'classnames';
import { Navbar, Box, Button, Notification, Title, Icon } from 'rbx';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSync } from '@fortawesome/free-solid-svg-icons';
import { Logo } from '../common/Logo';
import { ConfirmModal } from '../common/ConfirmModal';
import { withState, StateType, showVault, updateBookmark, setConfirmingResetBookmark, resetBookmark, saveBookmark, updateBookmarkErrors, getCategorySuggestions } from './state';
import { observer } from 'mobx-react-lite';
import { DateTime } from 'luxon';
import { ErrorBoundry } from '../common/ErrorBoundry';
import { FormField } from '../common/FormField';
import { CategoriesSelector } from '../common/CategorySelectors';
import './index.scss';

const App = withState(observer(({ state }: { state: StateType }) => {
  const changeName = useCallback((name: string) => updateBookmark({ name }), []);
  const changeNotes = useCallback((notes: string) => updateBookmark({ notes }), []);
  const confirmResetBookmark = useCallback(() => setConfirmingResetBookmark(true), []);
  const cancelResetBookmark = useCallback(() => setConfirmingResetBookmark(false), []);
  const updateBookmarkCategories = useCallback((categories: string[], errors: unknown) => { updateBookmark({ categories }); updateBookmarkErrors({ categories: errors }); }, []);
  const [version, updateVersion] = useState(0);

  useEffect(() => {
    const timeout = setTimeout(() => updateVersion(version + 1), 10 * 1000);
    return () => clearTimeout(timeout);
  });

  let syncRelativeTime = null;
  if (state.remoteStorage.lastSyncTime) {
    if (Date.now() - state.remoteStorage.lastSyncTime < 60 * 1000) {
      syncRelativeTime = 'just now';
    } else {
      syncRelativeTime = DateTime.fromMillis(state.remoteStorage.lastSyncTime).toRelative();
    }
  }

  let content;
  if (state.error) {
    content = <div className="error-message"><Notification color="danger">{ state.error }</Notification></div>;
  } else if (state.message) {
    content = <div className="message">{ state.message }</div>;
  } else if (state.editing) {
    content = <Box className={classnames('editing', state.editing.loading && 'is-loading', state.saving && 'saving')}>
      <Title>{state.editing.new ? 'New Bookmark' : 'Edit Bookmark'}: {state.editing.bookmark.url}</Title>
      <FormField className="name" size="medium" error={state.editing.errors.name} value={state.editing.bookmark.name} onChange={changeName} />
      <CategoriesSelector
        className="categories"
        label="Categories"
        categories={toJS(state.editing.bookmark.categories)}
        errors={toJS(state.editing.errors.categories)}
        onChange={updateBookmarkCategories}
        getCategorySuggestions={getCategorySuggestions}
      />
      <FormField className="notes" type="textarea" error={state.editing.errors.notes} value={state.editing.bookmark.notes} onChange={changeNotes} />
      <div className="actions">
        <Button className="reset" color="light" onClick={confirmResetBookmark}>Reset Changes</Button>
        <Button className="save" color="dark" onClick={saveBookmark}>{state.editing.new ? 'Create Bookmark' : 'Update Bookmark'}</Button>
      </div>
    </Box>;
  } else {
    content = <div className="loading"/>;
  }

  return <React.Fragment>
    <Navbar className="has-shadow" color="light">
      <Navbar.Brand>
        <Navbar.Item as="div">
          <Logo />
        </Navbar.Item>
      </Navbar.Brand>

      <Navbar.Segment align="end">
        { state.remoteStorage.syncError && <Navbar.Item as="div" className="remotestorage error">
          <div className="remotestorage error has-tooltip-bottom has-tooltip-danger has-tooltip-multiline" data-tooltip={`Sync failed: ${state.remoteStorage.syncError}`}>
            <Icon><FontAwesomeIcon icon={faSync} /></Icon>
          </div>
        </Navbar.Item> }
        { !state.remoteStorage.syncError && syncRelativeTime && <Navbar.Item as="div" className="remotestorage">
          <div className="remotestorage error has-tooltip-bottom has-tooltip-success" data-tooltip={`Synced ${syncRelativeTime}`}>
            <Icon><FontAwesomeIcon icon={faSync} /></Icon>
          </div>
        </Navbar.Item> }
        <Navbar.Item as="div">
          <Button color="dark" state={state.showingVault ? 'loading' : undefined} onClick={ showVault }><strong>View Vault</strong></Button>
        </Navbar.Item>
      </Navbar.Segment>
    </Navbar>

    { content }

    <ConfirmModal active={state.confirmingResetBookmark} prompt="Are you sure you want to discard your changes to this bookmark?" onConfirm={resetBookmark} onCancel={cancelResetBookmark} />
  </React.Fragment>;
}));

ReactDOM.render(<ErrorBoundry><App /></ErrorBoundry>, document.getElementById('container'));
