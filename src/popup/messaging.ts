import { sendMessage } from '../common/extension';
import { GetVaultStateResponse } from '../common/types/GetVaultStateResponse';
import getVaultStateResponseSchema from '../common/types/GetVaultStateResponse.json';
import { GetRemoteStorageStateResponse } from '../common/types/GetRemoteStorageStateResponse';
import getRemoteStorageStateResponseSchema from '../common/types/GetRemoteStorageStateResponse.json';
import { GetPopupStateResponse } from '../common/types/GetPopupStateResponse';
import getPopupStateResponseSchema from '../common/types/GetPopupStateResponse.json';
import { UpdatePopupStateRequest } from '../common/types/UpdatePopupStateRequest';
import { CreateBookmarkRequest } from '../common/types/CreateBookmarkRequest';
import { CreateBookmarkResponse } from '../common/types/CreateBookmarkResponse';
import createBookmarkResponseSchema from '../common/types/CreateBookmarkResponse.json';
import { UpdateBookmarkRequest } from '../common/types/UpdateBookmarkRequest';
import { UpdateBookmarkResponse } from '../common/types/UpdateBookmarkResponse';
import updateBookmarkResponseSchema from '../common/types/UpdateBookmarkResponse.json';
import { GetCategorySuggestionsRequest } from '../common/types/GetCategorySuggestionsRequest';
import { GetCategorySuggestionsResponse } from '../common/types/GetCategorySuggestionsResponse';
import getCategorySuggestionsResponseSchema from '../common/types/GetCategorySuggestionsResponse.json';
import emptyResponseSchema from '../common/types/EmptyResponse.json';
export { registerMessageHandler } from '../common/extension';

export async function getVaultState(): Promise<GetVaultStateResponse> {
  return sendMessage('getVaultState', undefined, getVaultStateResponseSchema);
}

export async function getRemoteStorageState(): Promise<GetRemoteStorageStateResponse> {
  return sendMessage('getRemoteStorageState', undefined, getRemoteStorageStateResponseSchema);
}

export async function getPopupState(): Promise<GetPopupStateResponse> {
  return sendMessage('getPopupState', undefined, getPopupStateResponseSchema);
}

export async function updatePopupState(request?: UpdatePopupStateRequest): Promise<void> {
  await sendMessage('updatePopupState', request, emptyResponseSchema);
}

export async function resetPopupState(): Promise<void> {
  await sendMessage('resetPopupState', undefined, emptyResponseSchema);
}

export async function showVault(): Promise<void> {
  await sendMessage('showVault', undefined, emptyResponseSchema);
}

export async function createBookmark(request?: CreateBookmarkRequest): Promise<CreateBookmarkResponse> {
  return sendMessage('createBookmark', request, createBookmarkResponseSchema);
}

export async function updateBookmark(request: UpdateBookmarkRequest): Promise<UpdateBookmarkResponse> {
  return sendMessage('updateBookmark', request, updateBookmarkResponseSchema);
}

export async function getCategorySuggestions(request: GetCategorySuggestionsRequest): Promise<GetCategorySuggestionsResponse> {
  return sendMessage('getCategorySuggestions', request, getCategorySuggestionsResponseSchema);
}
