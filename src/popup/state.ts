import _ from 'lodash';
import { observable, runInAction, toJS } from 'mobx';
import { Bookmark } from '../common/types/Bookmark';
import { VaultState } from '../common/types/VaultState';
import { registerMessageHandler, getVaultState, getPopupState, updatePopupState, resetPopupState, showVault as sendShowVault, createBookmark,
  updateBookmark as sendUpdateBookmark, getRemoteStorageState } from './messaging';
export { getCategorySuggestions } from './messaging';
import { EmptyRequest } from '../common/types/EmptyRequest';
import emptyRequestSchema from '../common/types/EmptyRequest.json';
import { VaultStateChangeRequest } from '../common/types/VaultStateChangeRequest';
import vaultStateChangeRequestSchema from '../common/types/VaultStateChangeRequest.json';
import { PopupStateChangeRequest } from '../common/types/PopupStateChangeRequest';
import popupStateChangeRequestSchema from '../common/types/PopupStateChangeRequest.json';
import { RemoteStorageStateChangeRequest } from '../common/types/RemoteStorageStateChangeRequest';
import remoteStorageStateChangeRequestSchema from '../common/types/RemoteStorageStateChangeRequest.json';
import { delay, makeWithState, assertNever, Resolved } from '../common/utils';

// ----------------------------------------------------------------------------
// Functions
// ----------------------------------------------------------------------------

export const showVault = displayErrors(async () => {
  runInAction(() => state.showingVault = true);
  await sendShowVault();
  runInAction(() => state.message = 'Opening vault...');
  window.close();
});

export const updateBookmark = displayErrors(async (changes: Partial<Bookmark & { categories: string[] }>) => {
  if (!state.editing) { return; }
  const updatedBookmark = _.assign(toJS(state.editing.bookmark), changes);
  runInAction(() => {
    state.editing!.errors.name = updatedBookmark.name.trim() ? undefined : 'You must enter a name';
    state.editing!.bookmark = updatedBookmark;
  });
  await updatePopupState({ bookmark: updatedBookmark });
});

export const updateBookmarkErrors = (errors: Partial<NonNullable<StateType['editing']>['errors']>) => {
  runInAction(() => state.editing ? state.editing.errors = _.assign(toJS(state.editing.errors), errors) : null);
};

export const setConfirmingResetBookmark = (confirming: boolean) => {
  runInAction(() => state.confirmingResetBookmark = confirming);
};

export const resetBookmark = displayErrors(async () => {
  if (!state.editing) { return; }
  runInAction(() => {
    state.editing!.loading = true;
    state.confirmingResetBookmark = false;
  });
  await resetPopupState();
  const response = await getPopupState();
  await handlePopupStateChange(response);
});

export const saveBookmark = displayErrors(async () => {
  if (!state.editing || hasErrors(state.editing.errors)) { return; }
  runInAction(() => {
    state.editing!.loading = true;
    state.editing!.bookmark.name = _.trim(state.editing!.bookmark.name);
  });
  const promise = state.editing.new ? createBookmark({ bookmark: toJS(state.editing.bookmark) }) : sendUpdateBookmark({ url: state.editing.bookmark.url, changes: toJS(state.editing.bookmark) });
  let response: Resolved<typeof promise>;
  try {
    response = await promise;
  } finally {
    runInAction(() => state.editing ? state.editing.loading = false : null);
  }
  if (response.errors) {
    runInAction(() => state.editing!.errors = _.assign(toJS(state.editing!.errors), response.errors));
  } else {
    runInAction(() => state.saving = true);
    await Promise.all([
      delay(3000),
      (async () => { await resetPopupState(); handlePopupStateChange(await getPopupState()); })(),
    ]);
    runInAction(() => state.saving = false);
  }
});

// ----------------------------------------------------------------------------
// State
// ----------------------------------------------------------------------------

export function displayErrors<Arguments extends unknown[], Result>(operation: (...args: Arguments) => Result): (...args: Arguments) => Promise<void> {
  return async (...args: Arguments) => {
    try {
      await operation(...args);
    } catch (error) {
      runInAction(() => state.error = (error as Error).message);
    }
  };
}

function hasErrors(errors: { [key: string]: unknown }) {
  return _.filter(_.values(errors)).length > 0;
}

export interface StateType {
  saving: boolean,
  showingVault: boolean,
  confirmingResetBookmark: boolean,
  editing?: {
    loading: boolean,
    new: boolean,
    bookmark: Bookmark & { categories: string[] },
    errors: {
      name?: string,
      notes?: string,
      categories?: unknown,
    },
  },
  remoteStorage: {
    lastSyncTime?: number,
    syncError?: string,
  },
  message?: string,
  error?: string,
}

const state: StateType = observable({
  saving: false,
  showingVault: false,
  confirmingResetBookmark: false,
  remoteStorage: {},
});

const handleVaultStateChange = (vaultState: VaultState) => {
  runInAction(() => {
    if (vaultState === 'none') {
      state.message = 'Your vault is locked.';
    } else if (vaultState === 'closed') {
      state.message = 'Your vault is locked.';
    } else if (vaultState === 'open') {
      if (state.message === 'Your vault is locked.') {
        state.message = undefined;
      }
    } else {
      assertNever(vaultState);
    }
  });
};

const handlePopupStateChange = (response: PopupStateChangeRequest) => {
  runInAction(() => {
    if (response.bookmark) {
      state.editing = {
        loading: false,
        new: response.new || false,
        bookmark: response.bookmark,
        errors: {},
      };
    } else {
      state.editing = undefined;
      state.message = 'You cannot bookmark this page.';
    }
  });
};

const handleRemoteStorageStateChange = (remoteStorageState: RemoteStorageStateChangeRequest) => {
  runInAction(() => state.remoteStorage = remoteStorageState);
};

displayErrors(async () => {
  handleVaultStateChange((await getVaultState()).state);
  handlePopupStateChange(await getPopupState());
  handleRemoteStorageStateChange(await getRemoteStorageState());
})();

function vaultStateChange(request: VaultStateChangeRequest) { handleVaultStateChange(request.state); }
registerMessageHandler('vaultStateChange', vaultStateChangeRequestSchema, vaultStateChange);

function popupStateChange(request: PopupStateChangeRequest) { handlePopupStateChange(request); }
registerMessageHandler('popupStateChange', popupStateChangeRequestSchema, popupStateChange);

function remoteStorageStateChange(request: RemoteStorageStateChangeRequest) { handleRemoteStorageStateChange(request); }
registerMessageHandler('remoteStorageStateChange', remoteStorageStateChangeRequestSchema, remoteStorageStateChange);

function closePopup(request: EmptyRequest) { window.close(); }
registerMessageHandler('closePopup', emptyRequestSchema, closePopup);

export const withState = makeWithState(state);
