import { Base64 } from 'js-base64';

async function parseEncryptionKey(passphrase: string, salt: Uint8Array, usage: Array<'encrypt' | 'decrypt'>) {
  const textEncoder = new TextEncoder();
  const encodedPassphrase = await crypto.subtle.importKey('raw', textEncoder.encode(passphrase), 'PBKDF2', false, ['deriveKey']);
  return crypto.subtle.deriveKey({ name: 'PBKDF2', salt, hash: 'SHA-256', iterations: 1000 }, encodedPassphrase, { name: 'AES-CBC', length: 256 }, true, usage);
}

export async function hashEncryptionKey(passphrase: string, salt?: string) {
  const finalSalt = salt ? Base64.toUint8Array(salt) : self.crypto.getRandomValues(new Uint8Array(16));
  const key = await parseEncryptionKey(passphrase, finalSalt, ['encrypt']);
  const hashed = new Uint8Array(await crypto.subtle.exportKey('raw', key));
  return `pbkdf2:${Base64.fromUint8Array(finalSalt)}:${Base64.fromUint8Array(hashed)}`;
}

export async function encryptData(passphrase: string, data: Uint8Array, salt?: string) {
  const finalSalt = salt ? Base64.toUint8Array(salt) : self.crypto.getRandomValues(new Uint8Array(16));
  const key = await parseEncryptionKey(passphrase, finalSalt, ['encrypt']);
  const encrypted = new Uint8Array(await self.crypto.subtle.encrypt({ name: 'AES-CBC', iv: finalSalt }, key, data));
  return `aes:${Base64.fromUint8Array(finalSalt)}:${Base64.fromUint8Array(encrypted)}`;
}

export async function decryptData(passphrase: string, data: string) {
  const parts = data.split(':');
  if (parts.length !== 3 || parts[0] !== 'aes') { throw new Error('Invalid encryption format!'); }
  const salt = Base64.toUint8Array(parts[1]);
  const key = await parseEncryptionKey(passphrase, salt, ['decrypt']);
  return new Uint8Array(await self.crypto.subtle.decrypt({ name: 'AES-CBC', iv: salt }, key, Base64.toUint8Array(parts[2])));
}
