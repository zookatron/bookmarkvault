import { sendMessageNoResponse } from '../common/extension';
import { VaultStateChangeRequest } from '../common/types/VaultStateChangeRequest';
import { PopupStateChangeRequest } from '../common/types/PopupStateChangeRequest';
import { RemoteStorageStateChangeRequest } from '../common/types/RemoteStorageStateChangeRequest';
export { registerMessageHandler } from '../common/extension';

export async function vaultStateChange(state: VaultStateChangeRequest): Promise<void> {
  sendMessageNoResponse('vaultStateChange', state);
}

export async function popupStateChange(state: PopupStateChangeRequest): Promise<void> {
  sendMessageNoResponse('popupStateChange', state);
}

export async function remoteStorageStateChange(state: RemoteStorageStateChangeRequest): Promise<void> {
  sendMessageNoResponse('remoteStorageStateChange', state);
}

export async function closePopup(): Promise<void> {
  sendMessageNoResponse('closePopup', {});
}
