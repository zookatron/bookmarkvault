import _ from 'lodash';
import msgpack from 'msgpack-lite';
import sjcl from 'sjcl';
import { getVersion, storageGet, storageSet, storageRemove } from '../common/extension';
import { hashEncryptionKey, encryptData } from './encryption';
import { putRemotestorageData, getRemotestorageData } from './remotestorage';
import { AnyObject, validate, validationErrors } from '../common/utils';
import { LocalStorageDataV1 } from '../common/types/LocalStorageDataV1';
import LocalStorageDataV1Schema from '../common/types/LocalStorageDataV1.json';
import { RemoteStorageDataV1 } from '../common/types/RemoteStorageDataV1';
import RemoteStorageDataV1Schema from '../common/types/RemoteStorageDataV1.json';
import { Cache } from '../common/types/Cache';

export async function updateStorageV1(passphrase: string) {
  await newStorageSave(passphrase, await oldStorageLoad(passphrase));
  await cleanUpOldStorage();
}

export async function updateRemoteStorageV1(passphrase: string, cache: NonNullable<Cache['remoteStorage']>) {
  const loaded = await getRemotestorageData(cache.credentials);
  const state = oldDecrypt(passphrase, JSON.parse(loaded.data));
  if (!validate<RemoteStorageDataV1>(state, RemoteStorageDataV1Schema)) {
    throw new Error(`Invalid RemoteStorage data structure: ${validationErrors(state, RemoteStorageDataV1Schema).join(', ')}`);
  }
  let saved;
  try {
    saved = await putRemotestorageData(cache.credentials, JSON.stringify({
      version: getVersion(),
      passphraseHash: await hashEncryptionKey(passphrase),
      data: await newEncrypt(passphrase, state),
    }), loaded.version);
  } catch (error) {
    throw new Error(`Unable to save data to RemoteStorage: ${(error as Error).message}`);
  }
  if (!saved.versionMatch) { throw new Error('Your RemoteStorage data was edited by an outside process since the last sync!'); }
}

async function oldStorageLoad(passphrase: string) {
  const { version, passphrase: passphraseHash, partNames } = await storageGet(['version', 'passphrase', 'partNames']);
  if (!version || !_.isString(passphraseHash) || !_.isArray(partNames)) { throw new Error('No vault data in storage!'); }
  if (!oldCheckPassphraseHash(passphrase, passphraseHash)) { throw new Error('Incorrect passphrase!'); }
  const items = await storageGet(partNames) as { [key: string]: string };
  const state = oldDecrypt(passphrase, _.map(_.sortBy(_.toPairs(items).map(([key, value]) => ({ key: Number(key.replace('part', '')), value })), 'key'), 'value'));
  if (!validate<LocalStorageDataV1>(state, LocalStorageDataV1Schema)) {
    throw new Error(`Invalid storage data structure: ${validationErrors(state, LocalStorageDataV1Schema).join(', ')}`);
  }
  return state;
}

async function cleanUpOldStorage() {
  const { partNames } = await storageGet(['partNames']);
  await storageRemove(['partNames'].concat(partNames as string[]));
}

async function newStorageSave(passphrase: string, state: LocalStorageDataV1) {
  return storageSet({
    version: '0.1.0',
    passphraseHash: await hashEncryptionKey(passphrase),
    data: await newEncrypt(passphrase, state),
  });
}

function oldCheckPassphraseHash(passphrase: string, passphraseHash: string) {
  const parts = passphraseHash.split(':');
  if (parts.length !== 3 || parts[0] !== 'pbkdf2') { throw new Error('Invalid passphrase hash format!'); }
  const salt = sjcl.codec.base64.toBits(parts[1]);
  const hashed = sjcl.misc.pbkdf2(passphrase, salt);
  const base64 = sjcl.codec.base64.fromBits(hashed);
  return base64 === parts[2];
}

function oldDecrypt(passphrase: string, parts: string[]) {
  try {
    let bits: sjcl.BitArray = [];
    const getSalt = _.memoize((salt: string) => sjcl.codec.base64.toBits(salt));
    const getCipher = _.memoize((salt: sjcl.BitArray) => new sjcl.cipher.aes(sjcl.misc.pbkdf2(passphrase, salt)));
    for (const part of parts) {
      const data = part.split(':');
      if (data.length !== 3 || data[0] !== 'aes') { throw new Error('Invalid encryption format!'); }
      const salt = getSalt(data[1]);
      const cipher = getCipher(salt);
      const encrypted = sjcl.codec.base64.toBits(data[2]);
      const chunk = sjcl.mode.ccm.decrypt(cipher, encrypted, salt);
      bits = sjcl.bitArray.concat(bits, chunk);
    }
    const bytes = sjcl.codec.bytes.fromBits(bits);
    const result = msgpack.decode(bytes);
    return result;
  } catch (error) {
    throw new Error(`Unable to decrypt vault data: ${(error as Error).message}`);
  }
}

async function newEncrypt(passphrase: string, data: AnyObject) {
  try {
    return encryptData(passphrase, msgpack.encode(data));
  } catch (error) {
    throw new Error(`Unable to encrypt vault data: ${(error as Error).message}`);
  }
}

/* tslint:disable */

// Source: https://raw.githubusercontent.com/bitwiseshiftleft/sjcl/master/core/codecBytes.js

/** @fileOverview Bit array codec implementations.
 *
 * @author Emily Stark
 * @author Mike Hamburg
 * @author Dan Boneh
 */

/**
 * Arrays of bytes
 * @namespace
 */
sjcl.codec.bytes = {
  /** Convert from a bitArray to an array of bytes. */
  fromBits: function (arr) {
    var out = [], bl = sjcl.bitArray.bitLength(arr), i, tmp = 0;
    for (i=0; i<bl/8; i++) {
      if ((i&3) === 0) {
        tmp = arr[i/4];
      }
      out.push(tmp >>> 24);
      tmp <<= 8;
    }
    return out;
  },
  /** Convert from an array of bytes to a bitArray. */
  toBits: function (bytes) {
    var out = [], i, tmp=0;
    for (i=0; i<bytes.length; i++) {
      tmp = tmp << 8 | bytes[i];
      if ((i&3) === 3) {
        out.push(tmp);
        tmp = 0;
      }
    }
    if (i&3) {
      out.push(sjcl.bitArray.partial(8*(i&3), tmp));
    }
    return out;
  }
};

/* tslint:enable */
