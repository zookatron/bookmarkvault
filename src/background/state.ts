import _ from 'lodash';
import normalizeUrl from 'normalize-url';
import { validateCategoryName, getCategoryLastName, getCategoryParent, getCategoryParents, withRollback, throwError, validate, validationErrors, assertNever, Resolved } from '../common/utils';
import { Markup, applyMarkup } from '../common/markup';
import { parseSearch } from '../common/search';
import Fuse from 'fuse.js';
import { Bookmark } from '../common/types/Bookmark';
import { HighlightedBookmark } from '../common/types/HighlightedBookmark';
import { Category } from '../common/types/Category';
import { HighlightedCategory } from '../common/types/HighlightedCategory';
import { Settings } from '../common/types/Settings';
import { Cache } from '../common/types/Cache';
import { VaultState } from '../common/types/VaultState';
import { getVersion, setPageAction, openTab, onActiveTabChanged, popupOpened } from '../common/extension';
import { getStorageVersion, storageLoad, storageSave, deleteStorage, remoteStorageLoad, remoteStorageSave, remoteStorageInit } from './storage';
import { registerMessageHandler, vaultStateChange, popupStateChange, remoteStorageStateChange, closePopup } from './messaging';
import { GetVaultStateResponse } from '../common/types/GetVaultStateResponse';
import { CreateVaultRequest } from '../common/types/CreateVaultRequest';
import createVaultRequestSchema from '../common/types/CreateVaultRequest.json';
import { OpenVaultRequest } from '../common/types/OpenVaultRequest';
import openVaultRequestSchema from '../common/types/OpenVaultRequest.json';
import { GetCategoryContentsRequest } from '../common/types/GetCategoryContentsRequest';
import getCategoryContentsRequestSchema from '../common/types/GetCategoryContentsRequest.json';
import { GetCategoryContentsResponse } from '../common/types/GetCategoryContentsResponse';
import { GetSearchResultsRequest } from '../common/types/GetSearchResultsRequest';
import getSearchResultsRequestSchema from '../common/types/GetSearchResultsRequest.json';
import { GetSearchResultsResponse } from '../common/types/GetSearchResultsResponse';
import { GetCategorySuggestionsRequest } from '../common/types/GetCategorySuggestionsRequest';
import getCategorySuggestionsRequestSchema from '../common/types/GetCategorySuggestionsRequest.json';
import { GetCategorySuggestionsResponse } from '../common/types/GetCategorySuggestionsResponse';
import { CreateBookmarkRequest } from '../common/types/CreateBookmarkRequest';
import createBookmarkRequestSchema from '../common/types/CreateBookmarkRequest.json';
import { CreateBookmarkResponse } from '../common/types/CreateBookmarkResponse';
import { EditBookmarkRequest } from '../common/types/EditBookmarkRequest';
import editBookmarkRequestSchema from '../common/types/EditBookmarkRequest.json';
import { EditBookmarkResponse } from '../common/types/EditBookmarkResponse';
import { UpdateBookmarkRequest } from '../common/types/UpdateBookmarkRequest';
import updateBookmarkRequestSchema from '../common/types/UpdateBookmarkRequest.json';
import { UpdateBookmarkResponse } from '../common/types/UpdateBookmarkResponse';
import { DeleteBookmarkRequest } from '../common/types/DeleteBookmarkRequest';
import deleteBookmarkRequestSchema from '../common/types/DeleteBookmarkRequest.json';
import { CreateCategoryRequest } from '../common/types/CreateCategoryRequest';
import createCategoryRequestSchema from '../common/types/CreateCategoryRequest.json';
import { CreateCategoryResponse } from '../common/types/CreateCategoryResponse';
import { UpdateCategoryRequest } from '../common/types/UpdateCategoryRequest';
import updateCategoryRequestSchema from '../common/types/UpdateCategoryRequest.json';
import { UpdateCategoryResponse } from '../common/types/UpdateCategoryResponse';
import { DeleteCategoryRequest } from '../common/types/DeleteCategoryRequest';
import deleteCategoryRequestSchema from '../common/types/DeleteCategoryRequest.json';
import { GetSettingsResponse } from '../common/types/GetSettingsResponse';
import { UpdateSettingsRequest } from '../common/types/UpdateSettingsRequest';
import updateSettingsRequestSchema from '../common/types/UpdateSettingsRequest.json';
import { UpdateRemoteStorageAccountRequest } from '../common/types/UpdateRemoteStorageAccountRequest';
import updateRemoteStorageAccountRequestSchema from '../common/types/UpdateRemoteStorageAccountRequest.json';
import { UpdateRemoteStorageAccountResponse } from '../common/types/UpdateRemoteStorageAccountResponse';
import { ConfirmUpdateRemoteStorageAccountRequest } from '../common/types/ConfirmUpdateRemoteStorageAccountRequest';
import confirmUpdateRemoteStorageAccountRequestSchema from '../common/types/ConfirmUpdateRemoteStorageAccountRequest.json';
import { GetRemoteStorageStateResponse } from '../common/types/GetRemoteStorageStateResponse';
import { GetPopupStateResponse } from '../common/types/GetPopupStateResponse';
import { UpdatePopupStateRequest } from '../common/types/UpdatePopupStateRequest';
import updatePopupStateRequestSchema from '../common/types/UpdatePopupStateRequest.json';
import { ExportJSONResponse } from '../common/types/ExportJSONResponse';
import { ImportJSONRequest } from '../common/types/ImportJSONRequest';
import importJSONRequestSchema from '../common/types/ImportJSONRequest.json';
import { EmptyRequest } from '../common/types/EmptyRequest';
import emptyRequestSchema from '../common/types/EmptyRequest.json';
import { JSONImportV1 } from '../common/types/JSONImportV1';
import JSONImportV1Schema from '../common/types/JSONImportV1.json';

// ----------------------------------------------------------------------------
// State
// ----------------------------------------------------------------------------

export interface StateType {
  passphrase: string | null,
  vaultOpen: boolean,
  bookmarks: Array<Bookmark & { categories: string[] }>,
  categories: Category[],
  indices: {
    bookmarkIndex: { [category: string]: Bookmark[] },
    categoryIndex: { [category: string]: Category[] },
    bookmarks: ReturnType<typeof getBookmarksIndex>,
    categories: ReturnType<typeof getCategoriesIndex>,
  },
  settings: Settings,
  cache: Cache,
  popup: {
    page: {
      url: string,
      title: string,
    } | null,
    changes?: Bookmark & { categories: string[] },
  },
  lockTimer: ReturnType<typeof setTimeout> | null,
}

const state: StateType = {} as StateType;
resetState();

const pageSize = 20;
let inProgress: Promise<unknown> = Promise.resolve();

function resetState() {
  _.assign(state, {
    passphrase: null,
    vaultOpen: false,
    bookmarks: [],
    categories: [],
    indices: {
      rootBookmarkIndex: [],
      bookmarkIndexes: {},
      rootCategoryIndex: [],
      categoryIndexes: {},
    },
    settings: {
      autoLockEnabled: false,
      autoLockTime: 5,
    },
    cache: {
      showHidden: false,
    },
    popup: { url: null },
    lockTimer: null,
  });
  refreshIndices();
}

// ----------------------------------------------------------------------------
// Indexes
// ----------------------------------------------------------------------------

function getBookmarksIndex() {
  const bookmarks = state.cache.showHidden ? state.bookmarks : state.bookmarks.filter(bookmark => _.every(bookmark.categories, category => !isHidden(category)));
  return {
    terms: new Fuse(bookmarks, {
      includeScore: true,
      includeMatches: true,
      findAllMatches: true,
      ignoreLocation: true,
      minMatchCharLength: 3,
      threshold: 0.3,
      keys: [
        { name: 'name', weight: 0.7 },
        { name: 'notes', weight: 0.2 },
        { name: 'url', weight: 0.1 },
      ],
    }),
    names: new Fuse(bookmarks, {
      includeScore: true,
      includeMatches: true,
      findAllMatches: true,
      ignoreLocation: true,
      minMatchCharLength: 3,
      threshold: 0.3,
      keys: ['name'],
    }),
    notes: new Fuse(bookmarks, {
      includeScore: true,
      includeMatches: true,
      findAllMatches: true,
      ignoreLocation: true,
      minMatchCharLength: 3,
      threshold: 0.3,
      keys: ['notes'],
    }),
    urls: new Fuse(bookmarks, {
      includeScore: true,
      includeMatches: true,
      findAllMatches: true,
      ignoreLocation: true,
      minMatchCharLength: 3,
      threshold: 0.3,
      keys: ['url'],
    }),
    categories: new Fuse(bookmarks, {
      includeScore: true,
      includeMatches: true,
      findAllMatches: true,
      ignoreLocation: true,
      minMatchCharLength: 3,
      threshold: 0.3,
      keys: ['categories'],
    }),
  };
}

function getCategoriesIndex() {
  const categories = state.categories.filter(category => !isHidden(category.name))
    .map(category => (_.assign({}, category, { extra: { lastname: getCategoryLastName(category.name), parent: getCategoryParent(category.name) } })));
  return {
    suggestions: new Fuse(categories, {
      includeMatches: true,
      findAllMatches: true,
      ignoreLocation: true,
      minMatchCharLength: 3,
      threshold: 0.3,
      keys: ['name'],
    }),
    names: new Fuse(categories, {
      includeScore: true,
      includeMatches: true,
      findAllMatches: true,
      ignoreLocation: true,
      minMatchCharLength: 3,
      threshold: 0.3,
      keys: ['extra.lastname'],
    }),
    notes: new Fuse(categories, {
      includeScore: true,
      includeMatches: true,
      findAllMatches: true,
      ignoreLocation: true,
      minMatchCharLength: 3,
      threshold: 0.3,
      keys: ['notes'],
    }),
    parents: new Fuse(categories, {
      includeScore: true,
      includeMatches: true,
      findAllMatches: true,
      ignoreLocation: true,
      minMatchCharLength: 3,
      threshold: 0.3,
      keys: ['extra.parent'],
    }),
  };
}

function refreshIndices() {
  const bookmarkIndex: { [name: string]: Array<Bookmark & { categories: string[] }> } = {};
  const categoryIndex: { [name: string]: Category[] } = {};

  categoryIndex[''] = [];
  bookmarkIndex[''] = [];

  for (let index = state.categories.length - 1; index >= 0; index--) {
    const category = state.categories[index];
    if (isHidden(category.name)) { continue; }
    if (category.name.indexOf('/') === -1) {
      categoryIndex[''].push(category);
    } else {
      const parent = getCategoryParent(category.name);
      if (!categoryIndex[parent]) { categoryIndex[parent] = []; }
      categoryIndex[parent].push(category);
    }
    if (!categoryIndex[category.name]) { categoryIndex[category.name] = []; }
    bookmarkIndex[category.name] = [];
  }

  for (let index = state.bookmarks.length - 1; index >= 0; index--) {
    const bookmark = state.bookmarks[index];
    if (bookmark.categories.length === 0) {
      bookmarkIndex[''].push(bookmark);
    } else {
      if (_.some(bookmark.categories, isHidden)) { continue; }
      for (let categoryPosition = bookmark.categories.length - 1; categoryPosition >= 0; categoryPosition--) {
        const category = bookmark.categories[categoryPosition];
        if (!bookmarkIndex[category]) { bookmarkIndex[category] = []; }
        bookmarkIndex[category].push(bookmark);
      }
    }
  }

  state.indices = {
    bookmarkIndex: _.mapValues(bookmarkIndex, bookmarks => _.reverse(_.sortBy(_.map(bookmarks, bookmark => _.omit(bookmark, 'categories')), 'created_at'))),
    categoryIndex: _.mapValues(categoryIndex, categories => _.reverse(_.sortBy(categories, 'created_at'))),
    bookmarks: getBookmarksIndex(),
    categories: getCategoriesIndex(),
  };
}

// ----------------------------------------------------------------------------
// Utilities
// ----------------------------------------------------------------------------

const isHidden = (category: string) => state.cache.showHidden ? false : _.includes(category.split('/'), 'Hidden') || _.includes(category.split('/'), 'hidden');

function addBookmark(bookmark: Bookmark & { categories: string[] }) {
  const position = state.bookmarks.length;
  state.bookmarks.push(bookmark);
  return () => state.categories.splice(position, 1);
}

function replaceBookmark(oldBookmark: Bookmark & { categories: string[] }, newBookmark: Bookmark & { categories: string[] }) {
  const index = _.indexOf(state.bookmarks, oldBookmark);
  if (index === -1) { throw new Error('Unable to find category location while attempting to replace category!'); }
  state.bookmarks[index] = newBookmark;
  return () => state.bookmarks[index] = oldBookmark;
}

function removeBookmark(bookmark: Bookmark & { categories: string[] }) {
  const index = _.indexOf(state.bookmarks, bookmark);
  if (index === -1) { throw new Error('Unable to find bookmark location while attempting to remove bookmark!'); }
  const [removedBookmark] = state.bookmarks.splice(index, 1);
  return () => state.bookmarks.splice(index, 0, removedBookmark);
}

function maybeAddCategoryAndParents(category: Category) {
  const position = state.categories.length;
  let amount = 0;
  if (!_.some(state.categories, { name: category.name })) {
    state.categories.push(category);
    amount += 1;
  }
  for (const parent of getCategoryParents(category.name)) {
    if (!_.some(state.categories, { name: parent })) {
      state.categories.push({ name: parent, image: null, notes: '', created_at: Date.now() });
      amount += 1;
    }
  }
  return () => state.categories.splice(position, amount);
}

function replaceCategory(oldCategory: Category, newCategory: Category) {
  const index = _.indexOf(state.categories, oldCategory);
  if (index === -1) { throw new Error('Unable to find category location while attempting to replace category!'); }
  state.categories[index] = newCategory;
  return () => state.categories[index] = oldCategory;
}

function removeCategory(category: Category) {
  const index = _.indexOf(state.categories, category);
  if (index === -1) { throw new Error('Unable to find category location while attempting to remove category!'); }
  const [removedCategory] = state.categories.splice(index, 1);
  return () => state.categories.splice(index, 0, removedCategory);
}

function findUnusedCategoryName(parent: string) {
  const categories = _.keys(state.indices.categoryIndex);
  for (let num = 1; true; num++) {
    const name = `${parent === '' ? '' : `${parent}/`}New Category${num === 1 ? '' : ` ${num}`}`;
    if (!_.includes(categories, name)) { return name; }
  }
}

const DataURLRegex = /^data:[a-z]+\/[a-z0-9-+.]+(;[a-z-]+=[a-z0-9-]+)?;base64,([a-z0-9!$&',()*+;=\-._~:@\/?%]*)$/i;

function combineMatches<T extends {}>(matches: Array<Fuse.FuseResult<T>>): { score: number, result: T, highlighted: T } {
  const result = matches[0].item;
  let score = 0;
  const highlightAdditions: { [key: string]: Markup[] } = {};

  for (const match of matches) {
    score += 1 - match.score!;
    for (const submatch of match.matches!) {
      const key = submatch.key === 'categories' ? `categories[${submatch.refIndex}]` : submatch.key || '';
      if (!highlightAdditions[key]) { highlightAdditions[key] = []; }
      highlightAdditions[key] = highlightAdditions[key].concat(submatch.indices.map((indexPair: [number, number]) => ({ start: indexPair[0], end: indexPair[1] + 1, class: 'highlighted' })));
    }
  }

  const highlighted = _.cloneDeep(result);
  _.forEach(highlightAdditions, (value, key) => {
    _.set(highlighted, key, applyMarkup(_.get(highlighted, key), value));
  });

  return { score: -score, result, highlighted };
}

function paginateResults<CategoryType extends Category, BookmarkType extends Bookmark>(categories: CategoryType[], bookmarks: BookmarkType[], page?: number) {
  const totalResults = categories.length + bookmarks.length;
  const pages = Math.max(1, Math.ceil(totalResults / pageSize));
  const realpage = Math.min(Math.max(page ? page : 0, 0), pages - 1);
  const start = realpage * pageSize;

  const pageCategories = start > categories.length ? [] : categories.slice(start, start + pageSize);
  const pageBookmarks = pageCategories.length >= pageSize ? [] : bookmarks.slice(Math.max(0, start - categories.length), start - categories.length + pageSize);

  return {
    categories: pageCategories,
    bookmarks: pageBookmarks,
    page: realpage,
    pages,
  };
}

function resetLockTimer() {
  if (state.lockTimer) { clearTimeout(state.lockTimer); }
  if (!state.settings.autoLockEnabled) { return; }
  state.lockTimer = setTimeout(() => lockVault({}), state.settings.autoLockTime * 60 * 1000);
}

async function deriveVaultState(): Promise<VaultState> {
  if (state.vaultOpen) {
    return 'open';
  } else {
    try {
      await getStorageVersion();
      return 'closed';
    } catch (error) {
      return 'none';
    }
  }
}

async function vaultStateChanged() {
  const vaultState = await deriveVaultState();
  await setPageAction(vaultState === 'open' ? 'popup' : 'tab');
  await vaultStateChange({ state: vaultState });
}

function remoteStorageStateChanged() {
  return remoteStorageStateChange(state.cache.remoteStorage ? {
    address: state.cache.remoteStorage.credentials.address,
    lastSyncTime: state.cache.remoteStorage.lastSyncTime,
    syncError: state.cache.remoteStorage.syncError,
  } : {});
}

function derivePopupState() {
  if (!state.popup.page) { return {}; }
  const bookmark = _.find(state.bookmarks, { url: state.popup.page.url });
  return {
    new: !bookmark,
    bookmark: state.popup.changes ? state.popup.changes : bookmark ? bookmark : {
      url: state.popup.page.url,
      name: state.popup.page.title,
      image: null,
      notes: '',
      created_at: Date.now(),
      categories: [],
    },
  };
}

function popupStateChanged() {
  return popupStateChange(derivePopupState());
}

export function waitForComplete<Arguments extends unknown[], Result>(operation: (...args: Arguments) => Result, rethrow?: boolean): (...args: Arguments) => Promise<Result> {
  return async (...args: Arguments) => {
    const oldInProgress = inProgress;
    const newInProgress = (async () => {
      try {
        await oldInProgress;
      } catch (error) {
        // Do nothing
      }
      return operation(...args);
    })();
    inProgress = newInProgress;
    return newInProgress;
  };
}

async function checkVaultOpen() {
  if (state.vaultOpen) {
    resetLockTimer();
  } else {
    throw new Error('Vault is not open!');
  }
}

async function loadState() {
  if (!state.passphrase) { throw new Error('Cannot load state: Vault is not open!'); }
  const storedState = await storageLoad(state.passphrase);
  state.bookmarks = storedState.bookmarks;
  state.categories = storedState.categories;
  state.settings = storedState.settings;
  state.cache = storedState.cache;
  refreshIndices();
}

let lastRemoteStorageSync = 0;
async function syncRemoteStorage() {
  if (!state.passphrase) { throw new Error('Cannot sync RemoteStorage: Vault is not open!'); }
  if (!state.cache.remoteStorage) { return; }
  const remoteStorage = await remoteStorageLoad(state.passphrase, state.cache.remoteStorage);
  state.cache.remoteStorage = remoteStorage.cache;
  if (remoteStorage.newData) {
    state.bookmarks = remoteStorage.newData.bookmarks;
    state.categories = remoteStorage.newData.categories;
    state.settings = remoteStorage.newData.settings;
    refreshIndices();
    await storageSave(state.passphrase, { bookmarks: state.bookmarks, categories: state.categories, settings: state.settings, cache: state.cache });
  }
  lastRemoteStorageSync = Date.now();
  await remoteStorageStateChanged();
}

async function saveState() {
  if (!state.passphrase) { throw new Error('Cannot save state: Vault is not open!'); }
  if (state.cache.remoteStorage) {
    state.cache.remoteStorage = await remoteStorageSave(state.passphrase, state.cache.remoteStorage, { bookmarks: state.bookmarks, categories: state.categories, settings: state.settings });
    await remoteStorageStateChanged();
  }
  await storageSave(state.passphrase, { bookmarks: state.bookmarks, categories: state.categories, settings: state.settings, cache: state.cache });
}

const getVaultState = waitForComplete(async (request: EmptyRequest): Promise<GetVaultStateResponse> => {
  if (state.vaultOpen && Date.now() - lastRemoteStorageSync > 10000) { await syncRemoteStorage(); }
  return { state: await deriveVaultState() };
});
registerMessageHandler('getVaultState', emptyRequestSchema, getVaultState);

// ----------------------------------------------------------------------------
// Message Handlers
// ----------------------------------------------------------------------------

const createVault = waitForComplete(async (request: CreateVaultRequest): Promise<void> => {
  resetState();
  state.passphrase = request.passphrase;
  await saveState();
  state.vaultOpen = true;
  resetLockTimer();
  vaultStateChanged();
});
registerMessageHandler('createVault', createVaultRequestSchema, createVault);

const openVault = waitForComplete(async (request: OpenVaultRequest): Promise<void> => {
  state.passphrase = request.passphrase;
  await loadState();
  await syncRemoteStorage();
  state.vaultOpen = true;
  resetLockTimer();
  await vaultStateChanged();
});
registerMessageHandler('openVault', openVaultRequestSchema, openVault);

const lockVault = waitForComplete(async (request: EmptyRequest): Promise<void> => {
  if (!state.vaultOpen) { return; }
  await saveState();
  await resetState();
  state.vaultOpen = false;
  await vaultStateChanged();
});
registerMessageHandler('lockVault', emptyRequestSchema, lockVault);

const deleteVault = waitForComplete(async (request: EmptyRequest): Promise<void> => {
  await checkVaultOpen();
  await deleteStorage();
  resetState();
  await vaultStateChanged();
});
registerMessageHandler('deleteVault', emptyRequestSchema, deleteVault);

const showVault = waitForComplete(async (request: EmptyRequest): Promise<void> => {
  await checkVaultOpen();
  await openTab();
});
registerMessageHandler('showVault', emptyRequestSchema, showVault);

const getCategoryContents = waitForComplete(async (request: GetCategoryContentsRequest): Promise<GetCategoryContentsResponse> => {
  await checkVaultOpen();
  const category = _.find(state.categories, { name: request.category }) || null;
  const categories = state.indices.categoryIndex[request.category];
  const bookmarks = state.indices.bookmarkIndex[request.category];
  if (!categories || !bookmarks) { throw new Error(`Unable to fetch contents for category "${request.category}"!`); }

  return { category, ...paginateResults(categories, bookmarks, request.page) };
});
registerMessageHandler('getCategoryContents', getCategoryContentsRequestSchema, getCategoryContents);

const getSearchResults = waitForComplete(async (request: GetSearchResultsRequest): Promise<GetSearchResultsResponse> => {
  await checkVaultOpen();

  if (!_.trim(request.search)) {
    return paginateResults([], [], request.page);
  }

  const search = parseSearch(request.search);

  const bookmarkSearches = _.flatten(search.map(term => {
    if (term.type === 'term') {
      return state.indices.bookmarks.terms.search(term.text);
    } else if (term.type === 'name') {
      return state.indices.bookmarks.names.search(term.text);
    } else if (term.type === 'notes') {
      return state.indices.bookmarks.notes.search(term.text);
    } else if (term.type === 'url') {
      return state.indices.bookmarks.urls.search(term.text);
    } else if (term.type === 'cat') {
      return state.indices.bookmarks.categories.search(term.text);
    } else {
      return [];
    }
  }));
  const bookmarks: HighlightedBookmark[] = _.map(_.sortBy(_.map(_.groupBy(_.filter(bookmarkSearches, match => !!match.matches?.length), 'item.url'), combineMatches), 'score'), result => {
    return _.assign({}, _.omit(result.result, 'categories'), { highlighted: _.omit(result.highlighted, 'categories') });
  });

  let categories: HighlightedCategory[] = [];
  if (!_.some(search, { type: 'url' })) {
    const categorySearches = _.flatten(search.map(term => {
      if (term.type === 'term') {
        return state.indices.categories.names.search(term.text);
      } else if (term.type === 'name') {
        return state.indices.categories.names.search(term.text);
      } else if (term.type === 'notes') {
        return state.indices.categories.notes.search(term.text);
      } else if (term.type === 'cat') {
        return state.indices.categories.parents.search(term.text);
      } else {
        return [];
      }
    }));
    categories = _.map(_.sortBy(_.map(_.groupBy(_.filter(categorySearches, match => !!match.matches?.length), 'item.name'), combineMatches), 'score'), result => {
      return _.assign({}, _.omit(result.result, 'extra'), {
        highlighted: _.assign(_.omit(result.highlighted, 'extra'), { name: `${result.highlighted.extra.parent ? `${result.highlighted.extra.parent}/` : ''}${result.highlighted.extra.lastname}` }),
      });
    });
  }

  return paginateResults(categories.slice(0, 3), bookmarks, request.page);
});
registerMessageHandler('getSearchResults', getSearchResultsRequestSchema, getSearchResults);

const getCategorySuggestions = waitForComplete(async (request: GetCategorySuggestionsRequest): Promise<GetCategorySuggestionsResponse> => {
  await checkVaultOpen();
  const suggestions = state.indices.categories.suggestions.search(request.category).slice(0, 8).map(result => {
    const highlights = _.flatten(_.map(result.matches, submatch => {
      return submatch.indices.map((indexPair: [number, number]) => ({ start: indexPair[0], end: indexPair[1] + 1, class: 'highlighted' }));
    }));
    return { name: result.item.name, highlightedName: applyMarkup(result.item.name, highlights) };
  });
  const existingParent = _.find(state.categories, { name: request.category })?.name || _.find(getCategoryParents(request.category), category => _.some(state.categories, { name: category }));
  return {
    existingParent: existingParent || '',
    suggestions,
  };
});
registerMessageHandler('getCategorySuggestions', getCategorySuggestionsRequestSchema, getCategorySuggestions);

const createCategory = waitForComplete(async (request: CreateCategoryRequest): Promise<CreateCategoryResponse> => {
  await checkVaultOpen();
  return withRollback(async addRollback => {
    const newCategory: Category = 'parent' in request ? {
      name: findUnusedCategoryName(request.parent),
      image: null,
      notes: '',
      created_at: Date.now(),
    } : _.assign({}, request.category, { created_at: Date.now() });

    // Initial validation
    if (!validateCategoryName(newCategory.name)) { throw new Error(`Invalid category name "${newCategory.name}"!`); }
    if (_.isString(newCategory.image) && !DataURLRegex.test(newCategory.image)) { throw new Error('Invalid category image data provided!'); }
    if (_.find(state.categories, { name: newCategory.name })) { throw new Error('Another category with this name already exists!'); }

    // Create category
    addRollback(maybeAddCategoryAndParents(newCategory));

    await saveState();
    refreshIndices();
    return {};
  });
});
registerMessageHandler('createCategory', createCategoryRequestSchema, createCategory);

const updateCategory = waitForComplete(async (request: UpdateCategoryRequest): Promise<UpdateCategoryResponse> => {
  await checkVaultOpen();
  return withRollback(async addRollback => {
    // Initial validation
    if (_.isString(request.changes.image) && !DataURLRegex.test(request.changes.image)) { throw new Error('Invalid category image data provided!'); }
    if (request.changes.name && request.name !== request.changes.name) {
      if (!validateCategoryName(request.changes.name)) { throw new Error(`Invalid category name "${request.changes.name}"!`); }
      if (_.find(state.categories, { name: request.changes.name })) { throw new Error('Another category with this name already exists!'); }
    }

    // Find existing category
    const oldCategory = _.find(state.categories, { name: request.name });
    if (!oldCategory) { throw new Error(`Unable to find category with name "${request.name}"!`); }

    // Update category
    const newCategory = _.assign({}, oldCategory, request.changes);
    addRollback(replaceCategory(oldCategory, newCategory));

    // Create parent categories & update bookmarks
    if (newCategory.name !== oldCategory.name) {
      addRollback(maybeAddCategoryAndParents(newCategory));

      state.categories.forEach(category => {
        if (_.startsWith(category.name, `${oldCategory.name}/`)) {
          addRollback(replaceCategory(category, _.assign({}, category, {
            name: `${newCategory.name}${category.name.slice(oldCategory.name.length)}`,
          })));
        }
      });

      state.bookmarks.forEach(bookmark => {
        if (_.some(bookmark.categories, category => category === oldCategory.name || _.startsWith(category, `${oldCategory.name}/`))) {
          addRollback(replaceBookmark(bookmark, _.assign({}, bookmark, {
            categories: _.map(bookmark.categories, bookmarkCategory => {
              if (bookmarkCategory === oldCategory.name || _.startsWith(bookmarkCategory, `${oldCategory.name}/`)) {
                return `${newCategory.name}${bookmarkCategory.slice(oldCategory.name.length)}`;
              } else {
                return bookmarkCategory;
              }
            }),
          })));
        }
      });
    }

    await saveState();
    refreshIndices();
    return {};
  });
});
registerMessageHandler('updateCategory', updateCategoryRequestSchema, updateCategory);

const deleteCategory = waitForComplete(async (request: DeleteCategoryRequest): Promise<void> => {
  await checkVaultOpen();
  return withRollback(async addRollback => {
    // Find existing category
    const category = _.find(state.categories, { name: request.name });
    if (!category) { throw new Error(`Unable to find category with name "${request.name}"!`); }

    // Don't delete categories with subcategories
    const subcategories = state.categories.filter(subcategory => _.startsWith(subcategory.name, `${category.name}/`));
    if (subcategories.length) { throw new Error(`Unable to delete category "${request.name}" because it has existing subcategories!`); }

    // Don't delete categories with bookmarks
    const bookmarks = state.bookmarks.filter(bookmark => _.some(bookmark.categories, bookmarkCategory => bookmarkCategory === category.name));
    if (bookmarks.length) { throw new Error(`Unable to delete category "${request.name}" because it has existing bookmarks!`); }

    // Delete category
    addRollback(removeCategory(category));

    await saveState();
    refreshIndices();
  });
});
registerMessageHandler('deleteCategory', deleteCategoryRequestSchema, deleteCategory);

const createBookmark = waitForComplete(async (request: CreateBookmarkRequest): Promise<CreateBookmarkResponse> => {
  await checkVaultOpen();
  return withRollback(async addRollback => {
    // Initial validation
    if (request.bookmark.name === '') { throw new Error('You must provide a bookmark name!'); }
    if (_.isString(request.bookmark.image) && !DataURLRegex.test(request.bookmark.image)) { throw new Error('Invalid bookmark image data provided!'); }
    if (_.find(state.bookmarks, { url: normalizeUrl(request.bookmark.url) })) { throw new Error('Another bookmark with this URL already exists!'); }
    request.bookmark.categories.forEach(categoryName => validateCategoryName(categoryName) ? null : throwError(`Invalid category name "${categoryName}"!`));

    // Create bookmark
    const newBookmark = _.clone(request.bookmark);
    newBookmark.url = normalizeUrl(newBookmark.url);
    newBookmark.created_at = Date.now();
    addRollback(addBookmark(newBookmark));

    // Create nonexistent categories
    newBookmark.categories.forEach(categoryName => addRollback(maybeAddCategoryAndParents({ name: categoryName, image: null, notes: '', created_at: Date.now() })));

    await saveState();
    refreshIndices();
    return {};
  });
});
registerMessageHandler('createBookmark', createBookmarkRequestSchema, createBookmark);

const editBookmark = waitForComplete(async (request: EditBookmarkRequest): Promise<EditBookmarkResponse> => {
  await checkVaultOpen();
  const bookmark = _.find(state.bookmarks, { url: request.url });
  if (!bookmark) { throw new Error(`Unable to find bookmark with URL "${request.url}"!`); }
  return { bookmark };
});
registerMessageHandler('editBookmark', editBookmarkRequestSchema, editBookmark);

const updateBookmark = waitForComplete(async (request: UpdateBookmarkRequest): Promise<UpdateBookmarkResponse> => {
  await checkVaultOpen();
  return withRollback(async addRollback => {
    // Initial validation
    if (request.changes.name && request.changes.name === '') { throw new Error('You must provide a bookmark name!'); }
    if (_.isString(request.changes.image) && !DataURLRegex.test(request.changes.image)) { throw new Error('Invalid bookmark image data provided!'); }
    if (request.changes.url) { request.changes.url = normalizeUrl(request.changes.url); }
    if (request.changes.url && request.url !== request.changes.url) {
      if (_.find(state.bookmarks, { url: request.changes.url })) { throw new Error('Another bookmark with this URL already exists!'); }
    }
    if (request.changes.categories) {
      request.changes.categories.forEach(categoryName => {
        if (!validateCategoryName(categoryName)) { throw new Error(`Invalid category name "${categoryName}"!`); }
      });
    }

    // Find existing bookmark
    const oldBookmark = _.find(state.bookmarks, { url: request.url });
    if (!oldBookmark) { throw new Error(`Unable to find bookmark with URL "${request.url}"!`); }

    // Update bookmark
    const newBookmark = _.assign({}, oldBookmark, request.changes);
    addRollback(replaceBookmark(oldBookmark, newBookmark));

    // Create nonexistent categories
    newBookmark.categories.forEach(categoryName => addRollback(maybeAddCategoryAndParents({ name: categoryName, image: null, notes: '', created_at: Date.now() })));

    await saveState();
    refreshIndices();
    return {};
  });
});
registerMessageHandler('updateBookmark', updateBookmarkRequestSchema, updateBookmark);

const deleteBookmark = waitForComplete(async (request: DeleteBookmarkRequest): Promise<void> => {
  await checkVaultOpen();
  return withRollback(async addRollback => {

    // Find existing bookmark
    const bookmark = _.find(state.bookmarks, { url: request.url });
    if (!bookmark) { throw new Error(`Unable to find bookmark with URL "${request.url}"!`); }

    // Delete bookmark
    addRollback(removeBookmark(bookmark));

    await saveState();
    refreshIndices();
  });
});
registerMessageHandler('deleteBookmark', deleteBookmarkRequestSchema, deleteBookmark);

const getSettings = waitForComplete(async (request: EmptyRequest): Promise<GetSettingsResponse> => {
  await checkVaultOpen();
  return { settings: _.assign({ showHidden: state.cache.showHidden }, state.settings) };
});
registerMessageHandler('getSettings', emptyRequestSchema, getSettings);

const updateSettings = waitForComplete(async (request: UpdateSettingsRequest): Promise<void> => {
  await checkVaultOpen();
  state.settings = _.assign(state.settings, _.omit(request, 'showHidden'));
  if (_.isBoolean(request.showHidden)) { state.cache.showHidden = request.showHidden; refreshIndices(); }
  await saveState();
});
registerMessageHandler('updateSettings', updateSettingsRequestSchema, updateSettings);

const updateRemoteStorageAccount = waitForComplete(async (request: UpdateRemoteStorageAccountRequest): Promise<UpdateRemoteStorageAccountResponse> => {
  await checkVaultOpen();
  confirmingRemoteStorageData = undefined;
  if (_.isNil(request.address)) {
    state.cache.remoteStorage = undefined;
    await remoteStorageStateChanged();
    await saveState();
    return { state: 'success' };
  } else {
    if (!request.address) { throw new Error('Invalid RemoteStorage address provided!'); }
    const credentials = await remoteStorageInit(request.address, state.passphrase!);
    const remoteStorage = await remoteStorageLoad(state.passphrase!, { credentials, lastSyncTime: Date.now() });
    if (remoteStorage.cache.syncError?.startsWith('Unable to decrypt vault data')) {
      confirmingRemoteStorageData = { state: 'overwrite-remote', remoteStorage };
      return { state: 'overwrite-remote' };
    } else if (remoteStorage.cache.syncError === 'No data found when loading from RemoteStorage') {
      state.cache.remoteStorage = remoteStorage.cache;
      await saveState();
      return { state: 'success' };
    } else if (remoteStorage.cache.syncError) {
      throw new Error(`Unable to access RemoteStorage data: ${remoteStorage.cache.syncError}`);
    } else if (state.bookmarks.length === 0 && state.categories.length === 0) {
      if (!remoteStorage.newData) { throw new Error('No RemoteStorage data or error returned from load process!'); }
      state.bookmarks = remoteStorage.newData.bookmarks;
      state.categories = remoteStorage.newData.categories;
      state.settings = remoteStorage.newData.settings;
      state.cache.remoteStorage = remoteStorage.cache;
      await storageSave(state.passphrase!, { bookmarks: state.bookmarks, categories: state.categories, settings: state.settings, cache: state.cache });
      await remoteStorageStateChanged();
      refreshIndices();
      return { state: 'success' };
    } else {
      confirmingRemoteStorageData = { state: 'choose-version', remoteStorage };
      return { state: 'choose-version' };
    }
  }
});
registerMessageHandler('updateRemoteStorageAccount', updateRemoteStorageAccountRequestSchema, updateRemoteStorageAccount);

let confirmingRemoteStorageData: { state: 'overwrite-remote' | 'choose-version', remoteStorage: Resolved<ReturnType<typeof remoteStorageLoad>> } | undefined = undefined;
const confirmUpdateRemoteStorageAccount = waitForComplete(async (request: ConfirmUpdateRemoteStorageAccountRequest): Promise<void> => {
  await checkVaultOpen();
  if (!confirmingRemoteStorageData) { throw new Error('Unable to proceed: There is no RemoteStorage setup in progress!'); }
  const { state: confirmingState, remoteStorage } = confirmingRemoteStorageData;
  confirmingRemoteStorageData = undefined;
  if (confirmingState === 'overwrite-remote') {
    if (request.action === 'cancel') {
      // Do Nothing
    } else if (request.action === 'confirm-overwrite-remote') {
      state.cache.remoteStorage = remoteStorage.cache;
      await saveState();
    } else {
      throw new Error(`Invalid response of "${request.action}" for current state of "${confirmingState}"!`);
    }
  } else if (confirmingState === 'choose-version') {
    if (request.action === 'cancel') {
      // Do Nothing
    } else if (request.action === 'choose-local-version') {
      state.cache.remoteStorage = remoteStorage.cache;
      await saveState();
    } else if (request.action === 'choose-remote-version') {
      if (!remoteStorage.newData) { throw new Error('No RemoteStorage data or error returned from load process!'); }
      state.bookmarks = remoteStorage.newData.bookmarks;
      state.categories = remoteStorage.newData.categories;
      state.settings = remoteStorage.newData.settings;
      state.cache.remoteStorage = remoteStorage.cache;
      await storageSave(state.passphrase!, { bookmarks: state.bookmarks, categories: state.categories, settings: state.settings, cache: state.cache });
      await remoteStorageStateChanged();
      refreshIndices();
    } else {
      throw new Error(`Invalid response of "${request.action}" for current state of "${confirmingState}"!`);
    }
  } else {
    assertNever(confirmingState);
  }
});
registerMessageHandler('confirmUpdateRemoteStorageAccount', confirmUpdateRemoteStorageAccountRequestSchema, confirmUpdateRemoteStorageAccount);

const getRemoteStorageState = waitForComplete(async (request: EmptyRequest): Promise<GetRemoteStorageStateResponse> => {
  await checkVaultOpen();
  if (!state.cache.remoteStorage) { return {}; }
  return {
    lastSyncTime: state.cache.remoteStorage.lastSyncTime,
    syncError: state.cache.remoteStorage.syncError,
    address: state.cache.remoteStorage.credentials.address,
  };
});
registerMessageHandler('getRemoteStorageState', emptyRequestSchema, getRemoteStorageState);

onActiveTabChanged((tab, lostAccess) => {
  if (lostAccess) {
    state.popup = { page: null };
    closePopup();
  } else if (!tab || !tab.url || (tab.url.substr(0, 7) !== 'http://' && tab.url.substr(0, 8) !== 'https://')) {
    state.popup = { page: null };
    popupStateChanged();
  } else {
    state.popup = { page: { url: normalizeUrl(tab.url), title: tab.title || '' } };
    popupStateChanged();
  }
});

const getPopupState = waitForComplete(async (request: EmptyRequest): Promise<GetPopupStateResponse> => {
  await popupOpened();
  await checkVaultOpen();
  return derivePopupState();
});
registerMessageHandler('getPopupState', emptyRequestSchema, getPopupState);

const updatePopupState = waitForComplete(async (request: UpdatePopupStateRequest): Promise<void> => {
  await checkVaultOpen();
  if (!state.popup.page || request.bookmark.url !== state.popup.page.url) { throw new Error('Invalid popup state!'); }
  state.popup.changes = request.bookmark;
});
registerMessageHandler('updatePopupState', updatePopupStateRequestSchema, updatePopupState);

const resetPopupState = waitForComplete(async (request: EmptyRequest): Promise<void> => {
  await checkVaultOpen();
  state.popup.changes = undefined;
});
registerMessageHandler('resetPopupState', emptyRequestSchema, resetPopupState);

const exportJSON = waitForComplete(async (request: EmptyRequest): Promise<ExportJSONResponse> => {
  return {
    export: JSON.stringify({
      bookmarks: state.bookmarks,
      categories: state.categories,
      settings: state.settings,
      version: '1',
    }),
  };
});
registerMessageHandler('exportJSON', emptyRequestSchema, exportJSON);

const importJSON = waitForComplete(async (request: ImportJSONRequest): Promise<void> => {
  await checkVaultOpen();
  const importedState = JSON.parse(request.import);
  if (!validate<JSONImportV1>(importedState, JSONImportV1Schema)) {
    throw new Error(`Invalid JSON import structure: ${validationErrors(importedState, JSONImportV1Schema).join(', ')}`);
  }

  importedState.categories.forEach(category => {
    if (!validateCategoryName(category.name)) { throw new Error(`Category "${JSON.stringify(category)}" in import has an invalid category name!`); }
    if (_.filter(importedState.categories, { name: category.name }).length !== 1) { throw new Error(`More than one category exists with name "${category.name}" in import!`); }
    getCategoryParents(category.name).forEach(parent => {
      if (!_.find(importedState.categories, { name: parent })) { throw new Error(`Category "${JSON.stringify(category)}" in import has a missing parent category "${parent}"!`); }
    });
  });

  importedState.bookmarks = importedState.bookmarks.map(bookmark => _.assign({}, bookmark, { url: normalizeUrl(bookmark.url) }));
  importedState.bookmarks.forEach(bookmark => {
    if (bookmark.url === '') { throw new Error(`Bookmark "${JSON.stringify(bookmark)}" in import has no url!`); }
    if (bookmark.name === '') { throw new Error(`Bookmark "${JSON.stringify(bookmark)}" in import has no name!`); }
    if (_.filter(importedState.bookmarks, { url: bookmark.url }).length !== 1) { throw new Error(`More than one bookmark exists with url "${bookmark.url}" in import!`); }
    bookmark.categories.forEach(categoryName => {
      if (!validateCategoryName(categoryName)) { throw new Error(`Bookmark "${JSON.stringify(bookmark)}" in import has invalid category name "${categoryName}"!`); }
      if (bookmark.categories.filter(bookmarkCategory => bookmarkCategory === categoryName).length > 1) { throw new Error(`Bookmark "${JSON.stringify(bookmark)}" in import has duplicate category "${categoryName}"!`); }
      if (!_.find(importedState.categories, { name: categoryName })) { throw new Error(`Bookmark "${JSON.stringify(bookmark)}" in import references nonexistent category name "${categoryName}"!`); }
    });
  });

  state.bookmarks = importedState.bookmarks;
  state.categories = importedState.categories;
  state.settings = importedState.settings;
  refreshIndices();
  return saveState();
});
registerMessageHandler('importJSON', importJSONRequestSchema, importJSON);
