import _ from 'lodash';
import msgpack from 'msgpack-lite';
import axios from 'axios';
import semver from 'semver';
import { getVersion, getOAuthRedirectURL, getOAuthResponse, storageGet, storageSet, storageClear } from '../common/extension';
import { AnyObject, validate, validationErrors } from '../common/utils';
import { Cache } from '../common/types/Cache';
import { hashEncryptionKey, encryptData, decryptData } from './encryption';
import { putRemotestorageData, getRemotestorageData } from './remotestorage';
import { RemoteStorageCredentials } from '../common/types/RemoteStorageCredentials';
import { LocalStorageDataV1 } from '../common/types/LocalStorageDataV1';
import LocalStorageDataV1Schema from '../common/types/LocalStorageDataV1.json';
import { RemoteStorageDataV1 } from '../common/types/RemoteStorageDataV1';
import RemoteStorageDataV1Schema from '../common/types/RemoteStorageDataV1.json';
import { updateStorageV1, updateRemoteStorageV1 } from './storageV1';

export async function getStorageVersion() {
  const data = await storageGet(['version']);
  if (data.version && _.isString(data.version)) {
    return data.version;
  } else {
    throw Error('No storage data!');
  }
}

export async function storageLoad(passphrase: string) {
  const { version } = await storageGet(['version']);
  if (!version || !_.isString(version)) { throw new Error('No vault data in storage1!'); }
  if (semver.lt(version, '0.1.0')) {
    await updateStorageV1(passphrase);
  }
  const { passphraseHash, data } = await storageGet(['passphraseHash', 'data']);
  if (!_.isString(passphraseHash) || !_.isString(data)) { throw new Error('No vault data in storage2!'); }
  if (!await checkPassphraseHash(passphrase, passphraseHash)) { throw new Error('Incorrect passphrase!'); }
  const state = await decrypt(passphrase, data);
  if (!validate<LocalStorageDataV1>(state, LocalStorageDataV1Schema)) {
    throw new Error(`Invalid storage data structure: ${validationErrors(state, LocalStorageDataV1Schema).join(', ')}`);
  }
  return state;
}

export async function storageSave(passphrase: string, state: LocalStorageDataV1) {
  return storageSet({
    version: getVersion(),
    passphraseHash: await hashEncryptionKey(passphrase),
    data: await encrypt(passphrase, state),
  });
}

export function deleteStorage() {
  return storageClear();
}

async function checkPassphraseHash(passphrase: string, passphraseHash: string) {
  const parts = passphraseHash.split(':');
  if (parts.length !== 3 || parts[0] !== 'pbkdf2') { throw new Error('Invalid passphrase hash format!'); }
  return await hashEncryptionKey(passphrase, parts[1]) === passphraseHash;
}

async function encrypt(passphrase: string, data: AnyObject) {
  try {
    return encryptData(passphrase, msgpack.encode(data));
  } catch (error) {
    throw new Error(`Unable to encrypt vault data: ${(error as Error).message}`);
  }
}

async function decrypt(passphrase: string, data: string) {
  try {
    return msgpack.decode(await decryptData(passphrase, data));
  } catch (error) {
    throw new Error(`Unable to decrypt vault data: ${(error as Error).message}`);
  }
}

export async function remoteStorageInit(address: string, passphrase: string): Promise<RemoteStorageCredentials> {
  const addressParts = address.split('@');
  if (addressParts.length !== 2) { throw new Error('Invalid RemoteStorage address provided!'); }
  const addressHost = addressParts[1];
  try {
    const response = await axios.get<AnyObject>(`https://${addressHost}/.well-known/webfinger?resource=${encodeURIComponent(`acct:${address}`)}`);
    if (!response.data || !response.data.links) { throw new Error(`Invalid WebFinger information found for account "${address}"!`); }
    const remoteStorage = _.find<AnyObject>(response.data.links, { rel: 'http://tools.ietf.org/id/draft-dejong-remotestorage' });
    if (!remoteStorage || !remoteStorage.href || !remoteStorage.properties) { throw new Error(`No RemoteStorage information found for account "${address}"!`); }
    const remoteStorageUrl = remoteStorage.href;
    const OAuthHost = remoteStorage.properties['http://tools.ietf.org/html/rfc6749#section-4.2'];
    if (!OAuthHost) { throw new Error(`No RemoteStorage OAuth URL found for account "${address}"!`); }
    const redirectUrl = getOAuthRedirectURL();
    const OAuthUrl = [
      OAuthHost,
      '?redirect_uri=' + encodeURIComponent(redirectUrl),
      '&scope=' + encodeURIComponent('bookmarkvault:rw'),
      '&client_id=' + encodeURIComponent(redirectUrl),
      '&response_type=token',
    ].join('');

    let responseURL;
    try {
      responseURL = await getOAuthResponse(OAuthUrl);
    } catch (error) {
      throw new Error(`Unable to perform OAuth flow: ${(error as Error).message}`);
    }
    const responseParts = responseURL.split('#access_token=');
    if (responseParts.length !== 2) { throw new Error(`Invalid access token response from "${addressHost}"!`); }
    return { address, url: remoteStorageUrl, accessToken: responseParts[1] };
  } catch (error) {
    throw new Error(`Unable to aquire RemoteStorage credentials: ${(error as Error).message}`);
  }
}

export async function remoteStorageLoad(passphrase: string, cache: Cache['remoteStorage']): Promise<{ cache: NonNullable<Cache['remoteStorage']>, newData?: RemoteStorageDataV1 }> {
  if (!cache) { throw new Error('No saved RemoteStorage credentials!'); }
  try {
    const data = await getRemotestorageData(cache.credentials, cache.version);
    const remoteStorageCache = { credentials: cache.credentials, version: data.version, lastSyncTime: Date.now() };
    if (data.versionMatch) { return { cache: remoteStorageCache }; }
    if (!data.data) { throw new Error('No data found when loading from RemoteStorage'); }
    let parsedJSON;
    try {
      parsedJSON = JSON.parse(data.data);
    } catch (error) {
      throw new Error('Invalid JSON data found when loading from RemoteStorage');
    }
    if (_.isArray(parsedJSON)) {
      await updateRemoteStorageV1(passphrase, cache);
      return remoteStorageLoad(passphrase, cache);
    }
    if (!_.isPlainObject(parsedJSON) ||
      !_.isString(parsedJSON.version) ||
      !_.isString(parsedJSON.passphraseHash) ||
      !_.isString(parsedJSON.data)) { throw new Error('No vault data in RemoteStorage!'); }
    if (semver.gt(parsedJSON.version, getVersion())) {
      throw new Error('Unable to load data from RemoteStorage: Your BookmarkVault version is out of date, please update the extension!');
    }
    if (!await checkPassphraseHash(passphrase, parsedJSON.passphraseHash)) { throw new Error('Incorrect passphrase!'); }
    const state = await decrypt(passphrase, parsedJSON.data);
    if (!validate<RemoteStorageDataV1>(state, RemoteStorageDataV1Schema)) {
      throw new Error(`Invalid RemoteStorage data structure: ${validationErrors(state, RemoteStorageDataV1Schema).join(', ')}`);
    }
    return { cache: remoteStorageCache, newData: state };
  } catch (error) {
    return { cache: _.assign({}, cache, { syncError: (error as Error).message }) };
  }
}

export async function remoteStorageSave(passphrase: string, cache: Cache['remoteStorage'], state: RemoteStorageDataV1): Promise<Cache['remoteStorage']> {
  if (!cache) { throw new Error('No saved RemoteStorage credentials!'); }
  let data;
  try {
    data = await putRemotestorageData(cache.credentials, JSON.stringify({
      version: getVersion(),
      passphraseHash: await hashEncryptionKey(passphrase),
      data: await encrypt(passphrase, state),
    }), cache.version);
  } catch (error) {
    throw new Error(`Unable to save data to RemoteStorage: ${(error as Error).message}`);
  }
  if (!data.versionMatch) { throw new Error('Your RemoteStorage data was edited by an outside process since the last sync!'); }
  return { credentials: cache.credentials, version: data.version, lastSyncTime: Date.now() };
}
