import _ from 'lodash';
import axios, {AxiosError} from 'axios';
import { RemoteStorageCredentials } from '../common/types/RemoteStorageCredentials';

export async function putRemotestorageData(credentials: RemoteStorageCredentials, data: string, version?: string): Promise<{ versionMatch: boolean, version?: string }> {
  try {
    const response = await axios.put(`${credentials.url}/bookmarkvault/data`, data, {
      headers: _.assign({ 'Authorization': 'Bearer ' + credentials.accessToken, 'Content-Type': 'text/plain' }, version ? { 'If-Match': version } : {}) as Record<string, string>,
    });
    return { versionMatch: true, version: response.headers.etag };
  } catch (error) {
    const axiosError = error as AxiosError;
    if (axiosError.response && axiosError.response.status === 412) {
      return { versionMatch: false, version };
    } else {
      throw new Error(`Unable to upload data to RemoteStorage server: ${axiosError.message}`);
    }
  }
}

export async function getRemotestorageData(credentials: RemoteStorageCredentials, version?: string): Promise<{ data: string, versionMatch: boolean, version?: string }> {
  try {
    const response = await axios.get(`${credentials.url}/bookmarkvault/data`, {
      headers: _.assign({ 'Authorization': 'Bearer ' + credentials.accessToken }, version ? { 'If-None-Match': version } : {}) as Record<string, string>,
      transformResponse: [_.identity],
    });
    return { data: response.data, versionMatch: false, version: response.headers.etag };
  } catch (error) {
    const axiosError = error as AxiosError;
    if (axiosError.response && axiosError.response.status === 304) {
      return { data: '', versionMatch: true, version };
    } else if (axiosError.response && axiosError.response.status === 404) {
      return { data: '', versionMatch: false, version: undefined };
    } else {
      throw new Error(`Unable to download data from RemoteStorage server: ${axiosError.message}`);
    }
  }
}

export async function headRemotestorageData(credentials: RemoteStorageCredentials): Promise<{ version?: string }> {
  try {
    const response = await axios.head(`${credentials.url}/bookmarkvault/data`, { headers: { 'Authorization': 'Bearer ' + credentials.accessToken }, transformResponse: [_.identity] });
    return { version: response.headers.etag };
  } catch (error) {
    const axiosError = error as AxiosError;
    if (axiosError.response && axiosError.response.status === 404) {
      return {};
    } else {
      throw new Error(`Unable to communicate with RemoteStorage server: ${axiosError.message}`);
    }
  }
}
