
function getThumbnail(imageUrl: string, size: number, quality: number) {
  return new Promise((resolve, reject) => {
    const image = new Image();
    image.onload = () => {
      const sourceSize = Math.min(image.naturalWidth, image.naturalHeight);
      const sourceX = (image.naturalWidth - sourceSize) / 2;
      const sourceY = (image.naturalHeight - sourceSize) / 2;
      const canvas = document.createElement('canvas');
      canvas.width = size;
      canvas.height = size;
      const context = canvas.getContext('2d');
      if (!context) { return reject(new Error('Unable to get image context!')); }
      context.drawImage(image, sourceX, sourceY, sourceSize, sourceSize, 0, 0, size, size);
      resolve(canvas.toDataURL('image/jpeg', quality));
    };
    image.onerror = () => reject(new Error('Unable to load image data!'));
    image.src = imageUrl;
  });
}

// tslint:disable-next-line:no-console
console.log(getThumbnail('http://i3.ytimg.com/vi/XPPtLNkVPWY/default.jpg', 64, 0.75));
