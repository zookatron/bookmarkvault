import React, { useCallback } from 'react';
import classnames from 'classnames';
import { observer } from 'mobx-react-lite';
import { Field, Input, Checkbox, Icon, Label, Title, Loader, Button } from 'rbx';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck, faExclamationCircle } from '@fortawesome/free-solid-svg-icons';
import { FormField } from '../common/FormField';
import { preventDefault } from '../common/utils';
import { FilePicker } from 'react-file-picker';
import { Layout } from './layout';
import { ConfirmModal } from '../common/ConfirmModal';
import { StateType, updateAutoLock, updateShowHidden, updateRemoteStorageAddress, editRemoteStorageAddress, cancelEditRemoteStorageAddress, saveRemoteStorageAddress, disableRemoteStorage,
  importJSON, importJSONError, exportJSON, confirmDeleteVault, deleteVault, stopConfirmingDeleteVault, confirmSaveRemoteStorageAddress } from './state';

export const SettingsPage = observer(({ state }: { state: StateType }) => {
  const page = state.settingsPage;
  const changeAutoLock = useCallback((event: React.ChangeEvent<HTMLInputElement>) => updateAutoLock(event.target.checked, page.settings.autoLockTime), [page.settings.autoLockTime]);
  const changeLockTime = useCallback((event: React.ChangeEvent<HTMLInputElement>) => updateAutoLock(page.settings.autoLockEnabled, event.target.value), [page.settings.autoLockEnabled]);
  const changeShowHidden = useCallback((event: React.ChangeEvent<HTMLInputElement>) => updateShowHidden(event.target.checked), [page.settings.showHidden]);
  const doEditRemoteStorageAddress = useCallback(preventDefault(editRemoteStorageAddress), []);
  const doCancelEditRemoteStorageAddress = useCallback(preventDefault(cancelEditRemoteStorageAddress), []);
  const doSaveRemoteStorageAddress = useCallback(preventDefault(saveRemoteStorageAddress), []);
  const doDisableRemoteStorage = useCallback(preventDefault(disableRemoteStorage), []);
  const cancelSaveRemoteStorageAddress = useCallback(() => confirmSaveRemoteStorageAddress('cancel'), []);
  const confirmOverwriteRemoteSaveRemoteStorageAddress = useCallback(() => confirmSaveRemoteStorageAddress('confirm-overwrite-remote'), []);
  const confirmLocalVersionSaveRemoteStorageAddress = useCallback(() => confirmSaveRemoteStorageAddress('choose-local-version'), []);
  const confirmRemoteVersionSaveRemoteStorageAddress = useCallback(() => confirmSaveRemoteStorageAddress('choose-remote-version'), []);

  let updatingLockTime = null;
  if (page.updatingLockTime && page.updatingLockTime.state === 'pending') {
    updatingLockTime = <span className="loading-status">Saving<Icon><Loader /></Icon></span>;
  } else if (page.updatingLockTime && page.updatingLockTime.state === 'fulfilled') {
    updatingLockTime = <span className="loading-status">Saved<Icon><FontAwesomeIcon icon={faCheck} /></Icon></span>;
  } else if (page.updatingLockTime && page.updatingLockTime.state === 'rejected') {
    updatingLockTime = <span className="loading-status">Error<Icon><FontAwesomeIcon icon={faExclamationCircle} /></Icon></span>;
  }

  let updatingShowHidden = null;
  if (page.updatingShowHidden && page.updatingShowHidden.state === 'pending') {
    updatingShowHidden = <span className="loading-status">Saving<Icon><Loader /></Icon></span>;
  } else if (page.updatingShowHidden && page.updatingShowHidden.state === 'fulfilled') {
    updatingShowHidden = <span className="loading-status">Saved<Icon><FontAwesomeIcon icon={faCheck} /></Icon></span>;
  } else if (page.updatingShowHidden && page.updatingShowHidden.state === 'rejected') {
    updatingShowHidden = <span className="loading-status">Error<Icon><FontAwesomeIcon icon={faExclamationCircle} /></Icon></span>;
  }

  let updatingRemoteStorage = null;
  if (page.updatingRemoteStorage && page.updatingRemoteStorage.state === 'pending') {
    updatingRemoteStorage = <span className="loading-status">Saving<Icon><Loader /></Icon></span>;
  } else if (page.updatingRemoteStorage && page.updatingRemoteStorage.state === 'fulfilled') {
    updatingRemoteStorage = <span className="loading-status">Saved<Icon><FontAwesomeIcon icon={faCheck} /></Icon></span>;
  } else if (page.updatingRemoteStorage && page.updatingRemoteStorage.state === 'rejected') {
    updatingRemoteStorage = <span className="loading-status">Error<Icon><FontAwesomeIcon icon={faExclamationCircle} /></Icon></span>;
  }
  const remoteStorageLoading = page.updatingRemoteStorage && page.updatingRemoteStorage.state === 'pending';

  let processingImportExport = null;
  if (page.importingJSON && page.importingJSON.state === 'pending') {
    processingImportExport = <span className="loading-status">Importing<Icon><Loader /></Icon></span>;
  } else if (page.importingJSON && page.importingJSON.state === 'fulfilled') {
    processingImportExport = <span className="loading-status">Imported successfully<Icon><FontAwesomeIcon icon={faCheck} /></Icon></span>;
  } else if (page.importingJSON && page.importingJSON.state === 'rejected') {
    processingImportExport = <span className="loading-status">Import failed<Icon><FontAwesomeIcon icon={faExclamationCircle} /></Icon></span>;
  } else if (page.exportingJSON && page.exportingJSON.state === 'pending') {
    processingImportExport = <span className="loading-status">Exporting<Icon><Loader /></Icon></span>;
  } else if (page.exportingJSON && page.exportingJSON.state === 'fulfilled') {
    processingImportExport = <span className="loading-status">Exported successfully<Icon><FontAwesomeIcon icon={faCheck} /></Icon></span>;
  } else if (page.exportingJSON && page.exportingJSON.state === 'rejected') {
    processingImportExport = <span className="loading-status">Export failed<Icon><FontAwesomeIcon icon={faExclamationCircle} /></Icon></span>;
  }

  return <Layout state={state} className={ classnames('settings', (page.loading || remoteStorageLoading) && 'is-loading') }>
    <Title size={4}>Automatic Locking{updatingLockTime}</Title>
    <Field className="autolock">
      <div className="autolock-enable">
        <Checkbox id="autoLock" className="switch is-rounded is-dark" checked={page.settings.autoLockEnabled} onChange={changeAutoLock} /><Label htmlFor="autoLock"/>
      </div>
      <div className={ classnames('autolock-locktime', !page.settings.autoLockEnabled && 'disabled') }>
        Lock after <Input
          type="text"
          color={page.errors.lockTime ? 'danger' : undefined}
          disabled={!page.settings.autoLockEnabled}
          value={page.settings.autoLockTime}
          onChange={changeLockTime}
        /> minutes inactive
      </div>
    </Field>
    <Title size={4}>Hidden Folders{updatingShowHidden}</Title>
    <Field className="show-hidden">
      <Checkbox id="showHidden" className="switch is-rounded is-dark" checked={page.settings.showHidden} onChange={changeShowHidden} />
      <Label htmlFor="showHidden">Show hidden folders?</Label>
    </Field>
    <Title size={4}>RemoteStorage Sync{updatingRemoteStorage}</Title>
    <form onSubmit={doSaveRemoteStorageAddress} className="remotestorage">
      RemoteStorage Address: { page.editingRemoteStorageAddress ? <FormField
        type="text"
        error={page.errors.remoteStorageAddress}
        placeholder="e.g. john.doe@example.com"
        disabled={remoteStorageLoading}
        value={page.settings.remoteStorageAddress}
        onChange={updateRemoteStorageAddress}
      /> : <span className="address">{ state.remoteStorage.address || '' }</span> }
      { page.editingRemoteStorageAddress ?
        <>
          <Button color="dark" disabled={remoteStorageLoading || !page.settings.remoteStorageAddress || page.errors.remoteStorageAddress}>Save</Button>
          <Button color="dark" disabled={remoteStorageLoading || !state.remoteStorage.address} onClick={doCancelEditRemoteStorageAddress}>Cancel</Button>
        </> : <>
          <Button color="dark" disabled={remoteStorageLoading} onClick={doEditRemoteStorageAddress}>Update</Button>
          <Button color="dark" disabled={remoteStorageLoading} onClick={doDisableRemoteStorage}>Disable</Button>
        </> }
    </form>
    <Title size={4}>JSON Import/Export{processingImportExport}</Title>
    <div className="json-import-export">
      <FilePicker style={{ display:'inline-block' }} extensions={['json']} onChange={importJSON} onError={importJSONError}><Button color="dark">Import JSON</Button></FilePicker>
      <Button color="dark" onClick={exportJSON}>Export JSON</Button>
      <p className="help">Use this tool to save or restore your vault data to or from a JSON file for backup purposes.</p>
    </div>
    <Title size={4}>Erase Vault Data</Title>
    <div className="erase-vault">
      <Button color="dark" onClick={confirmDeleteVault}>Erase</Button>
      <p className="help">This tool will completely erase your vault data.</p>
    </div>

    <ConfirmModal
      active={page.confirmingDeleteVault}
      loading={page.deletingVault}
      prompt="This will completely erase all vault data! This cannot be undone! Are you sure you want to do this?"
      onConfirm={deleteVault}
      onCancel={stopConfirmingDeleteVault}
    />
    <ConfirmModal
      active={page.confirmingSaveRemoteStorageAddress === 'overwrite-remote'}
      loading={page.confirmedSaveRemoteStorageAddress}
      prompt="This RemoteStorage account already has stored vault data encrypted with a different passphrase. Are you sure you want to overwite the data in this account?"
      onConfirm={confirmOverwriteRemoteSaveRemoteStorageAddress}
      onCancel={cancelSaveRemoteStorageAddress}
    />
    <ConfirmModal
      active={page.confirmingSaveRemoteStorageAddress === 'choose-version'}
      loading={page.confirmedSaveRemoteStorageAddress}
      prompt="This RemoteStorage account already has stored vault data. Do you want to use the remote version or the local version?"
      actions={[
        { label: 'Local Version', onClick: confirmLocalVersionSaveRemoteStorageAddress },
        { label: 'Remote Version', onClick: confirmRemoteVersionSaveRemoteStorageAddress },
        { label: 'Cancel', onClick: cancelSaveRemoteStorageAddress },
      ]}
    />
  </Layout>;
});
