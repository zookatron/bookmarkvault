import React, { useCallback, useState, useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import { DateTime } from 'luxon';
import { Navbar, Section, Container, Button, Title, Icon } from 'rbx';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCog, faSync } from '@fortawesome/free-solid-svg-icons';
import { StateType, showDashboardPage, showSettingsPage, setConfirmingLock, lockVault } from './state';
import { ConfirmModal } from '../common/ConfirmModal';
import { Logo } from '../common/Logo';

export const Layout = observer(({ state, className, children }: { state: StateType, className?: string, children: React.ReactNode[] | React.ReactNode }) => {
  const startConfirmingLock = useCallback(() => setConfirmingLock(true), []);
  const stopConfirmingLock = useCallback(() => setConfirmingLock(false), []);
  const onSettingsClick = useCallback(() => state.page !== 'settings' ? showSettingsPage() : null, [state.page]);
  const [version, updateVersion] = useState(0);

  useEffect(() => {
    const timeout = setTimeout(() => updateVersion(version + 1), 10 * 1000);
    return () => clearTimeout(timeout);
  });

  let syncRelativeTime = null;
  if (state.remoteStorage.lastSyncTime) {
    if (Date.now() - state.remoteStorage.lastSyncTime < 60 * 1000) {
      syncRelativeTime = 'just now';
    } else {
      syncRelativeTime = DateTime.fromMillis(state.remoteStorage.lastSyncTime).toRelative();
    }
  }

  return <React.Fragment>
    <Navbar className="has-shadow" color="light">
      <Container>
        <Navbar.Brand>
          <Navbar.Item onClick={showDashboardPage}>
            <Logo />
          </Navbar.Item>
        </Navbar.Brand>

        <Navbar.Segment align="end">
          { state.remoteStorage.syncError && <Navbar.Item as="div" className="remotestorage error">
            <div className="tooltip has-tooltip-bottom has-tooltip-danger has-tooltip-multiline" data-tooltip={state.remoteStorage.syncError}>
              <Icon><FontAwesomeIcon icon={faSync} /></Icon><Title size={6}>Sync failed!</Title>
            </div>
          </Navbar.Item> }
          { !state.remoteStorage.syncError && syncRelativeTime && <Navbar.Item as="div" className="remotestorage">
            <Icon><FontAwesomeIcon icon={faSync} /></Icon><Title size={6}>{ `Synced ${syncRelativeTime}` }</Title>
          </Navbar.Item> }
          <Navbar.Item as="div">
            <Button color="dark" onClick={ startConfirmingLock }><strong>Lock Vault</strong></Button>
          </Navbar.Item>
          <Navbar.Item active={state.page === 'settings'} onClick={onSettingsClick}>
            <Icon size="large"><FontAwesomeIcon icon={faCog} size="2x" /></Icon>
          </Navbar.Item>
        </Navbar.Segment>
      </Container>
    </Navbar>

    <Section>
      <Container className={className}>
        { children }
      </Container>
    </Section>

    <ConfirmModal active={state.confirmingLock} loading={state.locking} prompt="Are you sure you want to lock your vault?" onConfirm={lockVault} onCancel={stopConfirmingLock} />
  </React.Fragment>;
});
