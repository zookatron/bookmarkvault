import React, { useCallback } from 'react';
import classnames from 'classnames';
import { observer } from 'mobx-react-lite';
import { Container, Box, Field, Control, Input, Label, Button, Help } from 'rbx';
import { preventDefault } from '../common/utils';
import { Logo } from '../common/Logo';
import { StateType, updateCreateVaultPassphrase, createVault } from './state';

export const CreateVaultPage = observer(({ state }: { state: StateType }) => {
  const page = state.createVaultPage;
  const changePassphrase = useCallback((event: React.ChangeEvent<HTMLInputElement>) => updateCreateVaultPassphrase(event.target.value), []);
  const doCreateVault = useCallback(preventDefault(createVault), []);

  return <Container className={classnames('create-vault', page.loading && 'is-loading')}>
    <Box>
      <Logo size="large" text={<>Welcome to<br />BookmarkVault</>} />

      <form onSubmit={ doCreateVault }>
        <Field>
          <Label htmlFor="passphrase">Enter a passphrase to get started. <span className="example">e.g. &ldquo;I&rsquo;m the best 17-year old ever.&rdquo;</span></Label>
          <Control>
            <Input id="passphrase" autoFocus placeholder="Enter your new passphrase" value={page.passphrase} onChange={changePassphrase} />
            <Button color="dark"><strong>Create Vault</strong></Button>
          </Control>
        </Field>
      </form>

      <Help><a href="https://addons.mozilla.org/en-US/firefox/addon/bookmarkvault/" target="_blank">How does this work?</a></Help>
    </Box>
  </Container>;
});
