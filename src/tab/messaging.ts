import { sendMessage } from '../common/extension';
import { GetVaultStateResponse } from '../common/types/GetVaultStateResponse';
import getVaultStateResponseSchema from '../common/types/GetVaultStateResponse.json';
import { CreateVaultRequest } from '../common/types/CreateVaultRequest';
import { OpenVaultRequest } from '../common/types/OpenVaultRequest';
import { GetCategoryContentsRequest } from '../common/types/GetCategoryContentsRequest';
import { GetCategoryContentsResponse } from '../common/types/GetCategoryContentsResponse';
import getCategoryContentsResponseSchema from '../common/types/GetCategoryContentsResponse.json';
import { GetSearchResultsRequest } from '../common/types/GetSearchResultsRequest';
import { GetSearchResultsResponse } from '../common/types/GetSearchResultsResponse';
import getSearchResultsResponseSchema from '../common/types/GetSearchResultsResponse.json';
import { GetCategorySuggestionsRequest } from '../common/types/GetCategorySuggestionsRequest';
import { GetCategorySuggestionsResponse } from '../common/types/GetCategorySuggestionsResponse';
import getCategorySuggestionsResponseSchema from '../common/types/GetCategorySuggestionsResponse.json';
import { EditBookmarkRequest } from '../common/types/EditBookmarkRequest';
import { EditBookmarkResponse } from '../common/types/EditBookmarkResponse';
import editBookmarkResponseSchema from '../common/types/EditBookmarkResponse.json';
import { UpdateBookmarkRequest } from '../common/types/UpdateBookmarkRequest';
import { UpdateBookmarkResponse } from '../common/types/UpdateBookmarkResponse';
import updateBookmarkResponseSchema from '../common/types/UpdateBookmarkResponse.json';
import { DeleteBookmarkRequest } from '../common/types/DeleteBookmarkRequest';
import { CreateCategoryRequest } from '../common/types/CreateCategoryRequest';
import { CreateCategoryResponse } from '../common/types/CreateCategoryResponse';
import createCategoryResponseSchema from '../common/types/CreateCategoryResponse.json';
import { UpdateCategoryRequest } from '../common/types/UpdateCategoryRequest';
import { UpdateCategoryResponse } from '../common/types/UpdateCategoryResponse';
import updateCategoryResponseSchema from '../common/types/UpdateCategoryResponse.json';
import { DeleteCategoryRequest } from '../common/types/DeleteCategoryRequest';
import { GetSettingsResponse } from '../common/types/GetSettingsResponse';
import getSettingsResponseSchema from '../common/types/GetSettingsResponse.json';
import { UpdateSettingsRequest } from '../common/types/UpdateSettingsRequest';
import { UpdateRemoteStorageAccountRequest } from '../common/types/UpdateRemoteStorageAccountRequest';
import { UpdateRemoteStorageAccountResponse } from '../common/types/UpdateRemoteStorageAccountResponse';
import updateRemoteStorageAccountResponseSchema from '../common/types/UpdateRemoteStorageAccountResponse.json';
import { ConfirmUpdateRemoteStorageAccountRequest } from '../common/types/ConfirmUpdateRemoteStorageAccountRequest';
import { GetRemoteStorageStateResponse } from '../common/types/GetRemoteStorageStateResponse';
import getRemoteStorageStateResponseSchema from '../common/types/GetRemoteStorageStateResponse.json';
import { ExportJSONResponse } from '../common/types/ExportJSONResponse';
import exportJSONResponseSchema from '../common/types/ExportJSONResponse.json';
import { ImportJSONRequest } from '../common/types/ImportJSONRequest';
import emptyResponseSchema from '../common/types/EmptyResponse.json';
export { registerMessageHandler } from '../common/extension';

export async function getVaultState(): Promise<GetVaultStateResponse> {
  return sendMessage('getVaultState', undefined, getVaultStateResponseSchema);
}

export async function createVault(request: CreateVaultRequest): Promise<void> {
  await sendMessage('createVault', request, emptyResponseSchema);
}

export async function openVault(request: OpenVaultRequest): Promise<void> {
  await await sendMessage('openVault', request, emptyResponseSchema);
}

export async function lockVault(): Promise<void> {
  await sendMessage('lockVault', undefined, emptyResponseSchema);
}

export async function deleteVault(): Promise<void> {
  await sendMessage('deleteVault', undefined, emptyResponseSchema);
}

export async function getCategoryContents(request: GetCategoryContentsRequest): Promise<GetCategoryContentsResponse> {
  return sendMessage('getCategoryContents', request, getCategoryContentsResponseSchema);
}

export async function getSearchResults(request: GetSearchResultsRequest): Promise<GetSearchResultsResponse> {
  return sendMessage('getSearchResults', request, getSearchResultsResponseSchema);
}

export async function getCategorySuggestions(request: GetCategorySuggestionsRequest): Promise<GetCategorySuggestionsResponse> {
  return sendMessage('getCategorySuggestions', request, getCategorySuggestionsResponseSchema);
}

export async function editBookmark(request: EditBookmarkRequest): Promise<EditBookmarkResponse> {
  return sendMessage('editBookmark', request, editBookmarkResponseSchema);
}

export async function updateBookmark(request: UpdateBookmarkRequest): Promise<UpdateBookmarkResponse> {
  return sendMessage('updateBookmark', request, updateBookmarkResponseSchema);
}

export async function deleteBookmark(request: DeleteBookmarkRequest): Promise<void> {
  await sendMessage('deleteBookmark', request, emptyResponseSchema);
}

export async function createCategory(request: CreateCategoryRequest): Promise<CreateCategoryResponse> {
  return sendMessage('createCategory', request, createCategoryResponseSchema);
}

export async function updateCategory(request: UpdateCategoryRequest): Promise<UpdateCategoryResponse> {
  return sendMessage('updateCategory', request, updateCategoryResponseSchema);
}

export async function deleteCategory(request: DeleteCategoryRequest): Promise<void> {
  await sendMessage('deleteCategory', request, emptyResponseSchema);
}

export async function getSettings(): Promise<GetSettingsResponse> {
  return sendMessage('getSettings', undefined, getSettingsResponseSchema);
}

export async function updateSettings(request: UpdateSettingsRequest): Promise<void> {
  await sendMessage('updateSettings', request, emptyResponseSchema);
}

export async function updateRemoteStorageAccount(request: UpdateRemoteStorageAccountRequest): Promise<UpdateRemoteStorageAccountResponse> {
  return sendMessage('updateRemoteStorageAccount', request, updateRemoteStorageAccountResponseSchema);
}

export async function confirmUpdateRemoteStorageAccount(request: ConfirmUpdateRemoteStorageAccountRequest): Promise<void> {
  await sendMessage('confirmUpdateRemoteStorageAccount', request, emptyResponseSchema);
}

export async function getRemoteStorageState(): Promise<GetRemoteStorageStateResponse> {
  return sendMessage('getRemoteStorageState', undefined, getRemoteStorageStateResponseSchema);
}

export async function exportJSON(): Promise<ExportJSONResponse> {
  return sendMessage('exportJSON', undefined, exportJSONResponseSchema);
}

export async function importJSON(request: ImportJSONRequest): Promise<void> {
  await sendMessage('importJSON', request, emptyResponseSchema);
}
