import _ from 'lodash';
import React, { useCallback, useRef, useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import { toJS } from 'mobx';
import { FormField } from '../common/FormField';
import { Button, Icon, Title, List, Level, Field, Control, Breadcrumb } from 'rbx';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome, faSearch, faEdit, faPlus } from '@fortawesome/free-solid-svg-icons';
import { preventDefault, getCategoryParents, getCategoryLastName } from '../common/utils';
import { Category } from '../common/types/Category';
import { Layout } from './layout';
import { MarkupInput } from '../common/markup';
import { ConfirmModal } from '../common/ConfirmModal';
import { CategorySelector, CategoriesSelector } from '../common/CategorySelectors';
import { ImagePlaceholder } from '../common/ImagePlaceholder';
import { Paginator } from '../common/Paginator';
import { HTML } from '../common/HTML';
import { StateType, getCategorySuggestions, updateSearch, updatePage, showCategory, searchCategory, createCategory, deleteBookmark, confirmDeleteBookmark, cancelDeleteBookmark, editCategory, updateCategoryEdit,
  updateCategoryEditErrors, saveCategoryEdit, deleteCategory, confirmDeleteCategory, cancelDeleteCategory, editBookmark, updateBookmarkEdit, updateBookmarkEditErrors,
  saveBookmarkEdit, discardEdit, confirmDiscardEdit, cancelDiscardEdit } from './state';

export const DashboardPage = observer(({ state }: { state: StateType }) => {
  const page = state.dashboardPage;
  const showDashboard = useCallback(() => showCategory(null), []);
  const doShowCategory = useCallback(_.memoize((category: Category | string) => () => showCategory(category)), []);
  const doSearchCategory = useCallback(_.memoize((category: Category) => () => searchCategory(category)), []);

  const changeCategoryName = useCallback((name: string) => updateCategoryEdit({ name }), []);
  const changeCategoryNotes = useCallback((notes: string) => updateCategoryEdit({ notes }), []);
  const updateCategoryParent = useCallback((parent: string | undefined, error: unknown) => { if (!_.isNil(parent)) { updateCategoryEdit({ parent }); } updateCategoryEditErrors({ parent: error }); }, []);
  const doEditCategory = useCallback(_.memoize((category: string) => preventDefault(() => editCategory(category))), []);

  const changeBookmarkName = useCallback((name: string) => updateBookmarkEdit({ name }), []);
  const changeBookmarkUrl = useCallback((url: string) => updateBookmarkEdit({ url }), []);
  const changeBookmarkNotes = useCallback((notes: string) => updateBookmarkEdit({ notes }), []);
  const updateBookmarkCategories = useCallback((categories: string[], errors: unknown) => { updateBookmarkEdit({ categories }); updateBookmarkEditErrors({ categories: errors }); }, []);
  const doEditBookmark = useCallback(_.memoize((url: string) => preventDefault(() => editBookmark(url))), []);

  const markupInput = useRef<HTMLDivElement>(null);
  useEffect(() => markupInput.current?.focus(), [markupInput]);

  return <Layout state={state} className="dashboard">
    <div className="search">
      <Field kind="addons">
        <Control><Button rounded static size="medium">Search</Button></Control>
        <Control expanded><MarkupInput ref={markupInput} className="input is-rounded is-medium" onChange={updateSearch} value={page.search} markup={toJS(page.searchMarkup)} /></Control>
      </Field>
      <Button rounded size="medium" onClick={createCategory}><Icon><FontAwesomeIcon icon={faPlus} /></Icon>New Category</Button>
    </div>

    <div className={page.loading ? 'is-loading' : undefined}>
      { !page.category && !page.search && <>
        <div className="dashboard-header">
          <div className="image"><FontAwesomeIcon icon={faHome} /></div>
          <div className="details"><Title size={5}>Dashboard</Title></div>
        </div>
      </> }
      { page.search && <>
        <div className="search-header">
          <div className="image"><FontAwesomeIcon icon={faSearch} /></div>
          <div className="details"><Title size={5}>Search Results</Title></div>
        </div>
      </> }
      { page.category && !page.search && <>
        <Breadcrumb>
          <Breadcrumb.Item onClick={showDashboard} active={!page.category}>Dashboard</Breadcrumb.Item>
          { _.reverse(getCategoryParents(page.category.name)).map(category => {
            return <Breadcrumb.Item onClick={doShowCategory(category)} key={category}>{ getCategoryLastName(category) }</Breadcrumb.Item>;
          }) }
          <Breadcrumb.Item active>{ getCategoryLastName(page.category.name) }</Breadcrumb.Item>
        </Breadcrumb>
        <div className="category-header">
          <div className="image">{ page.category.image ? <img src={ page.category.image } /> : <ImagePlaceholder image="category" /> }</div>
          <div className="details">
            <Title size={5}><HTML html={getCategoryLastName(page.category.name)}/></Title>
            { page.category.notes && <Title subtitle size={6}><HTML html={page.category.notes}/></Title> }
          </div>
          <div className="actions"><Button color="light" onClick={doSearchCategory(page.category)}><Icon><FontAwesomeIcon icon={faSearch} /></Icon><span>Search</span></Button></div>
        </div>
      </> }

      { (!page.categories || !page.categories.length) && (!page.bookmarks || !page.bookmarks.length) && <div className="no-contents">{ page.search ? 'No matches' : 'No contents' }</div> }

      { page.categories && !!page.categories.length && <>
        <Title size={4}>Categories:</Title>
        <List className="contents">
          { page.categories.map(category => {
            const editing = page.editing && page.editing.type === 'category' && page.editing.name === category.name ? page.editing : null;
            return <React.Fragment key={category.name}>
              <List.Item as="div" className="content-view" onClick={doShowCategory(category)}>
                <div className="image">{ category.image ? <img src={ category.image } /> : <ImagePlaceholder image="category" /> }</div>
                <div className="details">
                  <Title size={5}><HTML html={getCategoryLastName(category.highlighted.name)}/></Title>
                  { category.notes && <Title subtitle size={6}><HTML html={category.highlighted.notes}/></Title> }
                </div>
                <div className="actions"><Button color="dark" onClick={doEditCategory(category.name)}><Icon><FontAwesomeIcon icon={faEdit} /></Icon><span>Edit</span></Button></div>
              </List.Item>
              { editing && <List.Item as="div" className="content-edit">
                <FormField className="name" label="Name" size="medium" error={editing.errors.name} value={editing.changes.name} onChange={changeCategoryName} />
                <CategorySelector
                  className="parent"
                  label="Parent"
                  category={editing.changes.parent}
                  errors={editing.errors.parent}
                  onChange={updateCategoryParent}
                  getCategorySuggestions={getCategorySuggestions}
                />
                <FormField className="notes" label="Notes" type="textarea" error={editing.errors.notes} value={editing.changes.notes} onChange={changeCategoryNotes} />
                <Field horizontal className="actions">
                  <Field.Label />
                  <Field.Body>
                    <Field>
                      <Level>
                        <Level.Item align="left"><Button onClick={confirmDeleteCategory} color="danger">Delete</Button></Level.Item>
                        <Level.Item align="right"><Button onClick={confirmDiscardEdit} color="info">Cancel</Button><Button onClick={saveCategoryEdit} color="success">Save</Button></Level.Item>
                      </Level>
                    </Field>
                  </Field.Body>
                </Field>
              </List.Item> }
            </React.Fragment>;
          }) }
        </List>
      </> }

      { page.bookmarks && !!page.bookmarks.length && <>
        <Title size={4}>Bookmarks:</Title>
        <List className="contents">
          { page.bookmarks.map(bookmark => {
            const editing = page.editing && page.editing.type === 'bookmark' && page.editing.url === bookmark.url ? page.editing : null;
            return <React.Fragment key={bookmark.url}>
              <List.Item className="content-view" href={bookmark.url} target="_blank">
                <div className="image">{ bookmark.image ? <img src={ bookmark.image } /> : <ImagePlaceholder image="bookmark" /> }</div>
                <div className="details">
                  <Title size={5}><HTML html={bookmark.highlighted.name}/></Title>
                  <Title subtitle size={6}><HTML html={bookmark.notes ? bookmark.highlighted.notes : bookmark.highlighted.url}/></Title>
                </div>
                <div className="actions"><Button color="dark" onClick={doEditBookmark(bookmark.url)}><Icon><FontAwesomeIcon icon={faEdit} /></Icon><span>Edit</span></Button></div>
              </List.Item>
              { editing && <List.Item as="div" className="content-edit">
                <FormField className="name" label="Name" size="medium" error={editing.errors.name} value={editing.changes.name} onChange={changeBookmarkName} />
                <FormField className="url" label="URL" error={editing.errors.url} value={editing.changes.url} onChange={changeBookmarkUrl} />
                <CategoriesSelector
                  className="categories"
                  label="Categories"
                  categories={toJS(editing.changes.categories)}
                  errors={toJS(editing.errors.categories)}
                  onChange={updateBookmarkCategories}
                  getCategorySuggestions={getCategorySuggestions}
                />
                <FormField className="notes" label="Notes" type="textarea" error={editing.errors.notes} value={editing.changes.notes} onChange={changeBookmarkNotes} />
                <Field horizontal className="actions">
                  <Field.Label />
                  <Field.Body>
                    <Field>
                      <Level>
                        <Level.Item align="left"><Button onClick={confirmDeleteBookmark} color="danger">Delete</Button></Level.Item>
                        <Level.Item align="right"><Button onClick={confirmDiscardEdit} color="info">Cancel</Button><Button onClick={saveBookmarkEdit} color="success">Save</Button></Level.Item>
                      </Level>
                    </Field>
                  </Field.Body>
                </Field>
              </List.Item> }
            </React.Fragment>;
          }) }
        </List>
      </> }

      <Paginator page={page.page} pages={page.pages} onChangePage={updatePage} />
    </div>

    <ConfirmModal active={page.confirmingDiscardEdit} prompt={`Are you sure you want to discard your changes to this ${page.editing && page.editing.type}?`} onConfirm={discardEdit} onCancel={cancelDiscardEdit} />
    <ConfirmModal active={Boolean(page.confirmingDeleteCategory)} prompt="Are you sure you want to delete this category?" onConfirm={deleteCategory} onCancel={cancelDeleteCategory} />
    <ConfirmModal active={Boolean(page.confirmingDeleteBookmark)} prompt="Are you sure you want to delete this bookmark?" onConfirm={deleteBookmark} onCancel={cancelDeleteBookmark} />
  </Layout>;
});
