import React, { useCallback } from 'react';
import classnames from 'classnames';
import { observer } from 'mobx-react-lite';
import { Container, Box, Field, Control, Input, Checkbox, Label, Button, Help } from 'rbx';
import { preventDefault } from '../common/utils';
import { Logo } from '../common/Logo';
import { StateType, updateOpenVaultPassphrase, updateOpenVaultPlaintext, openVault } from './state';

export const OpenVaultPage = observer(({ state }: { state: StateType }) => {
  const page = state.openVaultPage;
  const changePassphrase = useCallback((event: React.ChangeEvent<HTMLInputElement>) => updateOpenVaultPassphrase(event.target.value), []);
  const updatePlaintext = useCallback((event: React.ChangeEvent<HTMLInputElement>) => updateOpenVaultPlaintext(event.target.checked), []);
  const doOpenVault = useCallback(preventDefault(openVault), []);

  return <Container className={classnames('open-vault', page.loading && 'is-loading')}>
    <Box>
      <Logo size="large" text={<>Open your<br />BookmarkVault</>} />

      <form onSubmit={ doOpenVault }>
        <Field>
          <Label htmlFor="passphrase">Enter your passphrase:</Label>
          <Control>
            <div className="input-group">
              <Input id="passphrase" autoFocus type={page.plaintext ? 'text' : 'password'} placeholder="Enter your passphrase" value={page.passphrase} onChange={changePassphrase} />
              <div className="plaintext"><Label><Checkbox checked={page.plaintext} onChange={updatePlaintext} /> Show passphrase</Label></div>
            </div>
            <Button color="dark"><strong>Open Vault</strong></Button>
          </Control>
        </Field>
      </form>

      <Help><a href="https://addons.mozilla.org/en-US/firefox/addon/bookmarkvault/" target="_blank">How does this work?</a></Help>
    </Box>
  </Container>;
});
