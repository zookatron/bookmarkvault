import _ from 'lodash';
import { observable, runInAction, toJS } from 'mobx';
import { fromPromise, IPromiseBasedObservable } from 'mobx-utils';
import { Bookmark } from '../common/types/Bookmark';
import { HighlightedBookmark } from '../common/types/HighlightedBookmark';
import { Category } from '../common/types/Category';
import { HighlightedCategory } from '../common/types/HighlightedCategory';
import { VaultState } from '../common/types/VaultState';
import { registerMessageHandler, getVaultState, createVault as sendCreateVault, openVault as sendOpenVault, getSearchResults, getCategoryContents, updateBookmark, deleteBookmark as sendDeleteBookmark,
  editBookmark as sendEditBookmark, createCategory as sendCreateCategory, updateCategory, deleteCategory as sendDeleteCategory, getSettings, updateSettings, updateRemoteStorageAccount,
  lockVault as sendLockVault, importJSON as sendImportJSON, exportJSON as sendExportJSON, getRemoteStorageState, deleteVault as sendDeleteVault, confirmUpdateRemoteStorageAccount } from './messaging';
export { getCategorySuggestions } from './messaging';
import { Markup } from '../common/markup';
import { getSearchMarkup } from '../common/search';
import { VaultStateChangeRequest } from '../common/types/VaultStateChangeRequest';
import vaultStateChangeRequestSchema from '../common/types/VaultStateChangeRequest.json';
import { RemoteStorageStateChangeRequest } from '../common/types/RemoteStorageStateChangeRequest';
import remoteStorageStateChangeRequestSchema from '../common/types/RemoteStorageStateChangeRequest.json';
import { ConfirmUpdateRemoteStorageAccountRequest } from '../common/types/ConfirmUpdateRemoteStorageAccountRequest';
import { readFile, downloadFile, makeWithState, displayErrors, assertNever, getCategoryLastName, getCategoryParent, Resolved } from '../common/utils';
import AwesomeDebouncePromise from 'awesome-debounce-promise';

// ----------------------------------------------------------------------------
// State
// ----------------------------------------------------------------------------

export interface StateType {
  page: 'loading' | 'create_vault' | 'open_vault' | 'dashboard' | 'settings',
  createVaultPage: {
    loading: boolean,
    passphrase: string,
  },
  openVaultPage: {
    loading: boolean,
    passphrase: string,
    plaintext: boolean,
  },
  dashboardPage: {
    loading: boolean,
    category: Category | null,
    search: string,
    searchMarkup: Markup[],
    categories: HighlightedCategory[],
    bookmarks: HighlightedBookmark[],
    page: number,
    pages: number,
    editing: {
      type: 'category',
      name: string,
      changes: Category & { parent: string },
      changed: boolean,
      errors: {
        name?: string,
        image?: string,
        notes?: string,
        parent?: unknown,
      },
    } | {
      type: 'bookmark',
      url: string,
      changes: Bookmark & { categories: string[] },
      changed: boolean,
      errors: {
        url?: string,
        name?: string,
        image?: string,
        notes?: string,
        categories?: unknown,
      },
    } | null,
    confirmingDiscardEdit: boolean,
    doAfterConfirmDiscardEdit: (() => unknown) | null,
    confirmingDeleteCategory: { name: string } | null,
    confirmingDeleteBookmark: { url: string } | null,
  },
  settingsPage: {
    loading: boolean,
    editingRemoteStorageAddress: boolean,
    confirmingDeleteVault: boolean,
    deletingVault: boolean,
    confirmingSaveRemoteStorageAddress?: 'overwrite-remote' | 'choose-version',
    confirmedSaveRemoteStorageAddress: boolean,
    settings: {
      autoLockEnabled: boolean,
      autoLockTime: string,
      showHidden: boolean,
      remoteStorageAddress: string,
    },
    errors: {
      lockTime?: string,
      remoteStorageAddress?: string,
    },
    updatingLockTime?: IPromiseBasedObservable<void>,
    updatingShowHidden?: IPromiseBasedObservable<void>,
    updatingRemoteStorage?: IPromiseBasedObservable<void>,
    importingJSON?: IPromiseBasedObservable<void>,
    exportingJSON?: IPromiseBasedObservable<void>,
  },
  remoteStorage: {
    address?: string,
    lastSyncTime?: number,
    syncError?: string,
  },
  confirmingLock: boolean,
  locking: boolean,
}

const state = observable({
  page: 'loading',
  remoteStorage: {},
  confirmingLock: false,
  locking: false,
}) as unknown as StateType;

const handleVaultStateChange = displayErrors(async (vaultState: VaultState) => {
  if (vaultState === 'none') {
    showCreateVaultPage();
  } else if (vaultState === 'closed') {
    showOpenVaultPage();
  } else if (vaultState === 'open') {
    showDashboardPage();
    handleRemoteStorageStateChange(await getRemoteStorageState());
  } else {
    assertNever(vaultState);
  }
});

const handleRemoteStorageStateChange = (remoteStorageState: RemoteStorageStateChangeRequest) => {
  runInAction(() => state.remoteStorage = remoteStorageState);
};

displayErrors(async () => handleVaultStateChange((await getVaultState()).state))();

function vaultStateChange(request: VaultStateChangeRequest) { handleVaultStateChange(request.state); }
registerMessageHandler('vaultStateChange', vaultStateChangeRequestSchema, vaultStateChange);

function remoteStorageStateChange(request: RemoteStorageStateChangeRequest) { handleRemoteStorageStateChange(request); }
registerMessageHandler('remoteStorageStateChange', remoteStorageStateChangeRequestSchema, remoteStorageStateChange);

export const withState = makeWithState(state);

// ----------------------------------------------------------------------------
// Create Vault Page
// ----------------------------------------------------------------------------

export const showCreateVaultPage = () => {
  runInAction(() => {
    state.page = 'create_vault';
    state.createVaultPage = {
      loading: false,
      passphrase: '',
    };
  });
};

export const updateCreateVaultPassphrase = (passphrase: string) => {
  runInAction(() => state.createVaultPage.passphrase = passphrase);
};

export const createVault = displayErrors(async () => {
  runInAction(() => state.createVaultPage.loading = true);
  try {
    await sendCreateVault({ passphrase: state.createVaultPage.passphrase });
  } finally {
    runInAction(() => state.createVaultPage.loading = false);
  }
});

// ----------------------------------------------------------------------------
// Open Vault Page
// ----------------------------------------------------------------------------

export const showOpenVaultPage = () => {
  runInAction(() => {
    state.page = 'open_vault';
    state.openVaultPage = {
      loading: false,
      passphrase: '',
      plaintext: false,
    };
  });
};

export const updateOpenVaultPassphrase = (passphrase: string) => {
  runInAction(() => state.openVaultPage.passphrase = passphrase);
};

export const updateOpenVaultPlaintext = (plaintext: boolean) => {
  runInAction(() => state.openVaultPage.plaintext = plaintext);
};

export const openVault = displayErrors(async () => {
  runInAction(() => state.openVaultPage.loading = true);
  try {
    await sendOpenVault({ passphrase: state.openVaultPage.passphrase });
  } finally {
    runInAction(() => state.openVaultPage.loading = false);
  }
});

// ----------------------------------------------------------------------------
// Dasbhoard Page
// ----------------------------------------------------------------------------

export const showDashboardPage = (category?: string) => {
  runInAction(() => {
    state.page = 'dashboard';
    state.dashboardPage = {
      loading: true,
      category: null,
      search: '',
      searchMarkup: [],
      categories: [],
      bookmarks: [],
      page: 0,
      pages: 1,
      editing: null,
      confirmingDiscardEdit: false,
      doAfterConfirmDiscardEdit: null,
      confirmingDeleteCategory: null,
      confirmingDeleteBookmark: null,
    };
  });

  return refreshDashboardContents();
};


const refreshDashboardContents = async () => {
  runInAction(() => state.dashboardPage.loading = true);
  try {
    let response: Resolved<ReturnType<typeof getSearchResults>>;
    if (state.dashboardPage.search) {
      response = await getSearchResults({
        search: state.dashboardPage.search,
        page: state.dashboardPage.page,
      });
    } else {
      const categoryContents = await getCategoryContents({
        category: state.dashboardPage.category ? state.dashboardPage.category.name : '',
        page: state.dashboardPage.page,
      });
      response = _.assign({}, categoryContents, {
        categories: categoryContents.categories.map(category => _.assign({}, category, { highlighted: _.clone(category) })),
        bookmarks: categoryContents.bookmarks.map(bookmark => _.assign({}, bookmark, { highlighted: _.clone(bookmark) })),
      });
    }
    runInAction(() => {
      state.dashboardPage.loading = false;
      state.dashboardPage.editing = null;
      state.dashboardPage.categories = response.categories;
      state.dashboardPage.bookmarks = response.bookmarks;
      state.dashboardPage.page = response.page;
      state.dashboardPage.pages = response.pages;
    });
  } catch (error) {
    runInAction(() => state.dashboardPage.loading = false);
    throw error;
  }
};

const refreshCategoryContentsDebounced = AwesomeDebouncePromise(refreshDashboardContents, 100);

function autocompleteQuotes(newSearch: string, oldSearch: string) {
  const addingToEnd = newSearch.slice(0, newSearch.length - 1) === oldSearch.slice(0, oldSearch.length);
  const addingNewTerm = newSearch.length >= 2 && newSearch[newSearch.length - 1] === '"' && (newSearch[newSearch.length - 2] === ' ' || newSearch[newSearch.length - 2] === ':');
  const addingFirstTerm = newSearch.length === 1 && newSearch[0] === '"';
  let numQuotes = 0;
  for (let char = 0; char < newSearch.length; char++) {
    if (newSearch[char] === '"' && (char === 0 || newSearch[char - 1] !== '\\')) { numQuotes++; }
  }
  const hasUnpairedQuotes = numQuotes % 2 === 1;
  if (addingToEnd && (addingNewTerm || addingFirstTerm) && hasUnpairedQuotes) { return newSearch + '"'; }
  return newSearch;
}

const checkForEditing = <Arguments extends unknown[], Result>(operation: (...args: Arguments) => Result): (...args: Arguments) => Promise<void> => {
  return async (...args: Arguments) => {
    if (state.dashboardPage.editing && state.dashboardPage.editing.changed) {
      runInAction(() => {
        state.dashboardPage.confirmingDiscardEdit = true;
        state.dashboardPage.doAfterConfirmDiscardEdit = () => operation(...args);
      });
    } else {
      await operation(...args);
    }
  };
};

function hasErrors(errors: { [key: string]: unknown }) {
  return _.filter(_.values(errors)).length > 0;
}

export const updateSearch = checkForEditing(displayErrors(async (search: string) => {
  const finalSearch = autocompleteQuotes(search, state.dashboardPage.search);
  runInAction(() => {
    state.dashboardPage.search = finalSearch;
    state.dashboardPage.searchMarkup = getSearchMarkup(finalSearch);
    state.dashboardPage.page = 0;
  });
  await refreshCategoryContentsDebounced();
}));

export const updatePage = checkForEditing(displayErrors(async (page: number) => {
  runInAction(() => state.dashboardPage.page = page);
  await refreshDashboardContents();
}));

export const showCategory = checkForEditing(displayErrors(async (category: Category | string | null) => {
  runInAction(() => {
    state.dashboardPage.category = _.isString(category) ? { name: category, image: null, notes: '', created_at: Date.now() } : category;
    state.dashboardPage.page = 0;
    state.dashboardPage.search = '';
    state.dashboardPage.searchMarkup = [];
  });
  await refreshDashboardContents();
}));

export const searchCategory = checkForEditing(displayErrors(async (category: Category | null) => {
  runInAction(() => {
    state.dashboardPage.search += (state.dashboardPage.search.trim() ? ' ' : '') + (category ? `cat:${category.name} ` : `no-cat `);
    state.dashboardPage.searchMarkup = getSearchMarkup(state.dashboardPage.search);
    state.dashboardPage.page = 0;
  });
  await refreshDashboardContents();
}));

export const createCategory = displayErrors(async () => {
  runInAction(() => state.dashboardPage.loading = true);
  await sendCreateCategory({ parent: state.dashboardPage.category ? state.dashboardPage.category.name : '' });
  await refreshDashboardContents();
});

export const editCategory = checkForEditing(displayErrors((name: string) => {
  const category = _.find(state.dashboardPage.categories, { name });
  if (!category) { throw new Error('Unable to edit category: Category not found!'); }
  runInAction(() => {
    state.dashboardPage.editing = {
      type: 'category',
      name,
      changes: {
        name: getCategoryLastName(category.name),
        parent: getCategoryParent(category.name),
        image: category.image,
        notes: category.notes,
        created_at: category.created_at,
      },
      changed: false,
      errors: {},
    };
  });
}));

export const updateCategoryEdit = (changes: Partial<Category & { parent: string }>) => {
  runInAction(() => {
    if (!state.dashboardPage.editing || state.dashboardPage.editing.type !== 'category') { return; }
    state.dashboardPage.editing.changes = _.assign(toJS(state.dashboardPage.editing.changes), changes);
    state.dashboardPage.editing.changed = true;
    if (!state.dashboardPage.editing.changes.name.trim()) {
      state.dashboardPage.editing.errors.name = 'You must enter a name';
    } else if (state.dashboardPage.editing.changes.name !== state.dashboardPage.editing.changes.name.trim()) {
      state.dashboardPage.editing.errors.name = 'Invalid name';
    } else {
      state.dashboardPage.editing.errors.name = undefined;
    }
  });
};

export const updateCategoryEditErrors = (errors: Partial<Exclude<StateType['dashboardPage']['editing'], null>['errors']>) => {
  runInAction(() => {
    if (!state.dashboardPage.editing || state.dashboardPage.editing.type !== 'category') { return; }
    state.dashboardPage.editing.errors = _.assign(toJS(state.dashboardPage.editing.errors), errors);
  });
};

export const saveCategoryEdit = displayErrors(async () => {
  if (!state.dashboardPage.editing || state.dashboardPage.editing.type !== 'category' || hasErrors(state.dashboardPage.editing.errors)) { return; }
  runInAction(() => state.dashboardPage.loading = true);
  const editingChanges = toJS(state.dashboardPage.editing.changes);
  const changes = {
    name: `${editingChanges.parent === '' ? '' : `${editingChanges.parent}/`}${editingChanges.name}`,
    image: editingChanges.image,
    notes: editingChanges.notes,
  };
  try {
    const response = await updateCategory({ name: state.dashboardPage.editing.name, changes });
    if (response.errors) {
      runInAction(() => {
        if (!state.dashboardPage.editing || state.dashboardPage.editing.type !== 'category') { throw new Error('Unable to edit category: Category edit no longer active!'); }
        state.dashboardPage.loading = false;
        state.dashboardPage.editing.errors = _.assign(toJS(state.dashboardPage.editing.errors), response.errors);
      });
    } else {
      return refreshDashboardContents();
    }
  } catch (error) {
    runInAction(() => state.dashboardPage.loading = false);
    throw error;
  }
});

export const deleteCategory = displayErrors(async () => {
  if (!state.dashboardPage.confirmingDeleteCategory) { return; }
  runInAction(() => state.dashboardPage.loading = true);
  const name = state.dashboardPage.confirmingDeleteCategory.name;
  runInAction(() => state.dashboardPage.confirmingDeleteCategory = null);
  try {
    await sendDeleteCategory({ name });
  } catch (error) {
    runInAction(() => state.dashboardPage.loading = false);
    throw error;
  }
  await refreshDashboardContents();
});

export const confirmDeleteCategory = () => {
  runInAction(() => {
    if (!state.dashboardPage.editing || state.dashboardPage.editing.type !== 'category') { return; }
    state.dashboardPage.confirmingDeleteCategory = { name: state.dashboardPage.editing.name };
  });
};

export const cancelDeleteCategory = () => {
  runInAction(() => state.dashboardPage.confirmingDeleteCategory = null);
};

export const editBookmark = checkForEditing(displayErrors(async (url: string) => {
  runInAction(() => state.dashboardPage.loading = true);
  try {
    const response = await sendEditBookmark({ url });
    runInAction(() => {
      state.dashboardPage.loading = false;
      state.dashboardPage.editing = {
        type: 'bookmark',
        url,
        changes: response.bookmark,
        changed: false,
        errors: {},
      };
    });
  } catch (error) {
    runInAction(() => state.dashboardPage.loading = false);
    throw error;
  }
}));

export const updateBookmarkEdit = (changes: Partial<Bookmark & { categories: string[] }>) => {
  runInAction(() => {
    if (!state.dashboardPage.editing || state.dashboardPage.editing.type !== 'bookmark') { return; }
    state.dashboardPage.editing.changes = _.assign(toJS(state.dashboardPage.editing.changes), changes);
    state.dashboardPage.editing.changed = true;
    state.dashboardPage.editing.errors.name = state.dashboardPage.editing.changes.name.trim() ? undefined : 'You must enter a name';
    state.dashboardPage.editing.errors.url = state.dashboardPage.editing.changes.url.trim() ? undefined : 'You must enter a url';
  });
};

export const updateBookmarkEditErrors = (errors: Partial<NonNullable<StateType['dashboardPage']['editing']>['errors']>) => {
  runInAction(() => {
    if (!state.dashboardPage.editing || state.dashboardPage.editing.type !== 'bookmark') { return; }
    state.dashboardPage.editing.errors = _.assign(toJS(state.dashboardPage.editing.errors), errors);
  });
};

export const saveBookmarkEdit = displayErrors(async () => {
  if (!state.dashboardPage.editing || state.dashboardPage.editing.type !== 'bookmark' || hasErrors(state.dashboardPage.editing.errors)) { return; }
  runInAction(() => {
    state.dashboardPage.loading = true;
    state.dashboardPage.editing!.changes.name = _.trim(state.dashboardPage.editing!.changes.name);
  });
  try {
    const response = await updateBookmark({ url: state.dashboardPage.editing.url, changes: toJS(state.dashboardPage.editing.changes) });
    if (response.errors) {
      runInAction(() => {
        if (!state.dashboardPage.editing || state.dashboardPage.editing.type !== 'bookmark') { throw new Error('Unable to edit bookmark: Bookmark edit no longer active!'); }
        state.dashboardPage.loading = false;
        state.dashboardPage.editing.errors = _.assign(toJS(state.dashboardPage.editing.errors), response.errors);
      });
    } else {
      await refreshDashboardContents();
    }
  } catch (error) {
    runInAction(() => state.dashboardPage.loading = false);
    throw error;
  }
});

export const deleteBookmark = displayErrors(async () => {
  if (!state.dashboardPage.confirmingDeleteBookmark) { return; }
  runInAction(() => state.dashboardPage.loading = true);
  const url = state.dashboardPage.confirmingDeleteBookmark.url;
  runInAction(() => state.dashboardPage.confirmingDeleteBookmark = null);
  try {
    await sendDeleteBookmark({ url });
  } catch (error) {
    runInAction(() => state.dashboardPage.loading = false);
    throw error;
  }
  await refreshDashboardContents();
});

export const confirmDeleteBookmark = () => {
  runInAction(() => {
    if (!state.dashboardPage.editing || state.dashboardPage.editing.type !== 'bookmark') { return; }
    state.dashboardPage.confirmingDeleteBookmark = { url: state.dashboardPage.editing.url };
  });
};

export const cancelDeleteBookmark = () => {
  runInAction(() => state.dashboardPage.confirmingDeleteBookmark = null);
};

export const discardEdit = async () => {
  runInAction(() => {
    state.dashboardPage.confirmingDiscardEdit = false;
    state.dashboardPage.editing = null;
  });
  if (state.dashboardPage.doAfterConfirmDiscardEdit) {
    const doAfter = state.dashboardPage.doAfterConfirmDiscardEdit;
    runInAction(() => state.dashboardPage.doAfterConfirmDiscardEdit = null);
    await doAfter();
  }
};

export const confirmDiscardEdit = () => {
  runInAction(() => {
    if (state.dashboardPage.editing && state.dashboardPage.editing.changed) {
      state.dashboardPage.confirmingDiscardEdit = true;
    } else {
      state.dashboardPage.editing = null;
    }
  });
};

export const cancelDiscardEdit = () => {
  runInAction(() => {
    state.dashboardPage.confirmingDiscardEdit = false;
    state.dashboardPage.doAfterConfirmDiscardEdit = null;
  });
};



// ----------------------------------------------------------------------------
// Settings Page
// ----------------------------------------------------------------------------

export const showSettingsPage = displayErrors(async () => {
  runInAction(() => {
    state.page = 'settings';
    state.settingsPage = {
      loading: true,
      editingRemoteStorageAddress: true,
      confirmingDeleteVault: false,
      deletingVault: false,
      confirmingSaveRemoteStorageAddress: undefined,
      confirmedSaveRemoteStorageAddress: false,
      settings: {
        autoLockEnabled: false,
        autoLockTime: '',
        showHidden: false,
        remoteStorageAddress: '',
      },
      errors: {},
    };
  });

  try {
    const response = await getSettings();
    handleRemoteStorageStateChange(await getRemoteStorageState());
    runInAction(() => {
      state.settingsPage.loading = false;
      state.settingsPage.editingRemoteStorageAddress = !state.remoteStorage.address;
      state.settingsPage.settings = {
        autoLockEnabled: response.settings.autoLockEnabled,
        autoLockTime: response.settings.autoLockTime.toString(),
        showHidden: response.settings.showHidden,
        remoteStorageAddress: state.remoteStorage.address || '',
      };
    });
  } catch (error) {
    runInAction(() => state.dashboardPage.loading = false);
    throw error;
  }
});

export const updateAutoLock = (autoLockEnabled: boolean, autoLockTime: string) => {
  runInAction(() => {
    state.settingsPage.settings.autoLockEnabled = autoLockEnabled;
    state.settingsPage.settings.autoLockTime = autoLockTime;
    state.settingsPage.errors.lockTime = (isNaN(Number(autoLockTime)) || Number(autoLockTime) < 1 || autoLockTime.match(/[^0-9]/g)) ? 'Invalid lock time' : undefined;
    if (!state.settingsPage.errors.lockTime) {
      state.settingsPage.updatingLockTime = fromPromise(displayErrors(() => updateSettings({ autoLockEnabled, autoLockTime: Number(autoLockTime) }), true)());
    }
  });
};

export const updateShowHidden = (showHidden: boolean) => {
  runInAction(() => {
    state.settingsPage.settings.showHidden = showHidden;
    state.settingsPage.updatingShowHidden = fromPromise(displayErrors(() => updateSettings({ showHidden }), true)());
  });
};

export const updateRemoteStorageAddress = (remoteStorageAddress: string) => {
  runInAction(() => {
    state.settingsPage.settings.remoteStorageAddress = remoteStorageAddress;
    state.settingsPage.errors.remoteStorageAddress = !remoteStorageAddress.match(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/) ? 'Invalid address' : undefined;
  });
};

export const editRemoteStorageAddress = () => {
  runInAction(() => {
    state.settingsPage.settings.remoteStorageAddress = state.remoteStorage.address || '';
    state.settingsPage.editingRemoteStorageAddress = true;
  });
};

export const cancelEditRemoteStorageAddress = () => {
  runInAction(() => state.settingsPage.editingRemoteStorageAddress = false);
};

export const saveRemoteStorageAddress = () => {
  runInAction(() => {
    if (state.settingsPage.errors.remoteStorageAddress) { return; }
    state.settingsPage.updatingRemoteStorage = fromPromise(displayErrors(async () => {
      try {
        const response = await updateRemoteStorageAccount({ address: state.settingsPage.settings.remoteStorageAddress });
        runInAction(() => {
          if (response.state === 'success') {
            state.settingsPage.editingRemoteStorageAddress = false;
          } else {
            state.settingsPage.confirmingSaveRemoteStorageAddress = response.state;
          }
        });
      } catch (error) {
        runInAction(() => state.settingsPage.errors.remoteStorageAddress = (error as Error).message);
        throw error;
      }
    })());
  });
};

export const confirmSaveRemoteStorageAddress = displayErrors(async (action: ConfirmUpdateRemoteStorageAccountRequest['action']) => {
  runInAction(() => state.settingsPage.confirmedSaveRemoteStorageAddress = true);
  try {
    await confirmUpdateRemoteStorageAccount({ action });
  } catch (error) {
    runInAction(() => state.settingsPage.errors.remoteStorageAddress = (error as Error).message);
    throw error;
  } finally {
    runInAction(() => {
      state.settingsPage.confirmingSaveRemoteStorageAddress = undefined;
      state.settingsPage.confirmedSaveRemoteStorageAddress = false;
    });
  }
  runInAction(() => {
    if (action === 'cancel') {
      state.settingsPage.updatingRemoteStorage = undefined;
    } else {
      state.settingsPage.editingRemoteStorageAddress = false;
    }
  });
});

export const disableRemoteStorage = () => {
  runInAction(() => {
    state.settingsPage.updatingRemoteStorage = fromPromise(displayErrors(async () => {
      try {
        await updateRemoteStorageAccount({ address: null });
        runInAction(() => {
          state.settingsPage.settings.remoteStorageAddress = '';
          state.settingsPage.editingRemoteStorageAddress = true;
        });
      } catch (error) {
        runInAction(() => state.settingsPage.errors.remoteStorageAddress = (error as Error).message);
        throw error;
      }
    })());
  });
};

export const importJSON = (file: File) => {
  runInAction(() => {
    state.settingsPage.exportingJSON = undefined;
    state.settingsPage.importingJSON = fromPromise(displayErrors(async () => sendImportJSON({ import: await readFile(file) }), true)());
  });
};

export const importJSONError = (error: string) => displayErrors(async () => { throw new Error(error); })();

export const exportJSON = () => {
  runInAction(() => {
    state.settingsPage.importingJSON = undefined;
    state.settingsPage.exportingJSON = fromPromise(displayErrors(async () => downloadFile('bookmarkvault.json', 'application/json', (await sendExportJSON()).export), true)());
  });
};

export const confirmDeleteVault = () => {
  runInAction(() => state.settingsPage.confirmingDeleteVault = true);
};

export const deleteVault = displayErrors(async () => {
  runInAction(() => state.settingsPage.deletingVault = true);
  await sendDeleteVault();
});

export const stopConfirmingDeleteVault = () => {
  runInAction(() => state.settingsPage.confirmingDeleteVault = false);
};


// ----------------------------------------------------------------------------
// Other
// ----------------------------------------------------------------------------

export const setConfirmingLock = (confirming: boolean) => {
  runInAction(() => state.confirmingLock = confirming);
};

export const lockVault = displayErrors(async () => {
  runInAction(() => state.locking = true);
  try {
    await sendLockVault();
    runInAction(() => {
      state.confirmingLock = false;
      state.locking = false;
    });
  } catch (error) {
    runInAction(() => state.locking = false);
    throw error;
  }
});
