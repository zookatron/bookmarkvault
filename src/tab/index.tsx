import React from 'react';
import ReactDOM from 'react-dom';
import { toast, Zoom } from 'react-toastify';
import { assertNever } from '../common/utils';
import { withState, StateType } from './state';
import { observer } from 'mobx-react-lite';
import { ErrorBoundry } from '../common/ErrorBoundry';
import { CreateVaultPage } from './createVault';
import { OpenVaultPage } from './openVault';
import { DashboardPage } from './dashboard';
import { SettingsPage } from './settings';
import './index.scss';

const App = withState(observer(({ state }: { state: StateType }) => {
  if (state.page === 'loading') {
    return  <div className="loading-page"/>;
  } else if (state.page === 'create_vault') {
    return <CreateVaultPage state={state} />;
  } else if (state.page === 'open_vault') {
    return <OpenVaultPage state={state} />;
  } else if (state.page === 'dashboard') {
    return <DashboardPage state={state} />;
  } else if (state.page === 'settings') {
    return <SettingsPage state={state} />;
  } else {
    return assertNever(state.page);
  }
}));

toast.configure({
  autoClose: 30000,
  hideProgressBar: true,
  transition: Zoom,
});

ReactDOM.render(<ErrorBoundry><App /></ErrorBoundry>, document.getElementById('container'));
