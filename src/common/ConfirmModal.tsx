import React from 'react';
import { Button, Title, Modal, Notification, Delete, Level } from 'rbx';

export function ConfirmModal({ active, loading, prompt, onConfirm, onCancel, actions }: {
  active: boolean,
  loading?: boolean,
  prompt: string,
  onConfirm?: () => unknown,
  onCancel?: () => unknown,
  actions?: Array<{ label: string, onClick: () => unknown }>
}) {
  return <Modal className="confirm-modal" active={active}>
    <Modal.Background onClick={onCancel} />
    <Modal.Content className={loading ? 'is-loading' : ''} >
      <Notification>
        <Delete as="button" onClick={onCancel} />
        <Title size={4} textAlign="centered">{prompt}</Title>
        <Level>
          { actions ? <>
            { actions.map(({ label, onClick }) => <Level.Item key={label}><Button onClick={onClick} size="medium" color="dark">{label}</Button></Level.Item>) }
          </> : <>
            <Level.Item><Button onClick={onConfirm} size="medium" color="dark">Yes</Button></Level.Item>
            <Level.Item><Button onClick={onCancel} size="medium" color="dark">No</Button></Level.Item>
          </> }
        </Level>
      </Notification>
    </Modal.Content>
  </Modal>;
}
