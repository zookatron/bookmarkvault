import _ from 'lodash';
import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar, faFolder } from '@fortawesome/free-solid-svg-icons';

export function ImagePlaceholder({ image }: { image: 'bookmark' | 'category' }) {
  return <div className="image-placeholder"><FontAwesomeIcon icon={image === 'bookmark' ? faStar : faFolder} /></div>;
}
