@import '~normalize.css/normalize.css';
@import '~react-toastify/scss/_variables.scss';
@import '~react-toastify/scss/_toastContainer.scss';
@import '~react-toastify/scss/animations/_zoom.scss';
@import '~bulma/bulma.sass';
@import '~bulmaswatch/simplex/bulmaswatch.scss';
@import '~bulma-switch/src/sass/index.sass';
@import '~@creativebulma/bulma-tooltip/src/sass/index.sass';

%clear-fix {
  &::after {
    content: "";
    display: table;
    clear: both;
  }
}

a {
  outline: none;
}

button:-moz-focusring {
  outline: none;
}

input:-moz-focusring {
  outline: none;
}

.is-loading {
  position: relative;
  pointer-events: none;
  opacity: 0.5;
  min-height: 6rem;
  &:after {
    @include loader;
    position: absolute;
    top: calc(50% - 2rem);
    left: calc(50% - 2rem);
    width: 4rem;
    height: 4rem;
    border-width: 0.4rem;
  }
}

.button.is-loading {
  min-height: auto;
}

.navbar.is-light {
  @media screen and (min-width: 800px) {
    padding: 0 1.5rem;
  }

  .navbar-start > a.navbar-item.is-active {
    background: darken(#e8e8e8, 5);
  }

  .navbar-item {
    display: flex;
  }

  .navbar-end > .navbar-item.remotestorage {
    color: $green;

    .tooltip {
      display: flex;
      align-items: center;

      &:before {
        font-size: 1em;
        text-align: center;
      }
    }

    .title {
      color: inherit;
      padding-left: 0.3em;
    }

    &.error {
      color: $red;
    }
  }
}

.logo {
  @extend .title;

  display: flex;
  align-items: center;
  justify-content: center;
  padding-right: 0.5rem;

  .icon {
    height: 3rem;
    width: 3rem;
    margin-right: 0.6rem;

    img {
      width: 2.5rem;
      height: 2.5rem;
      max-height: none;
      max-width: none;
    }
  }

  .text {
    text-align: left;
  }

  &.is-large {
    .icon {
      height: 5rem;
      width: 5rem;

      img {
        width: 4.5rem;
        height: 4.5rem;
      }
    }
  }
}

.pagination {
  .pagination-list {
    justify-content: center;
    order: 2;

    .pagination-link.is-current {
      border: 1px solid #252525;
      background-color: #363636;
      color: whitesmoke;
    }
  }

  .pagination-previous {
    order: 1;
  }

  .pagination-previous[disabled], .pagination-next[disabled], .pagination-link[disabled] {
    pointer-events: none;
  }
}

.switch[type="checkbox"] + label {
  padding-top: 0;
  font-weight: normal;

  &:before,
  &:after {
    outline: none;
  }
}

.Toastify__toast-container {
  color: inherit;

  .Toastify__toast {
    @extend .notification;
    padding: .85rem 2rem .85rem 1rem;

    .Toastify__toast-body {
      display: flex;
      align-items: center;
    }

    .Toastify__toast-icon {
      margin-inline-end: 10px;
      width: 20px;
      flex-shrink: 0;
      display: flex;
    }

    &.Toastify__toast--error {
      @extend .is-danger;

      .Toastify__close-button {
        background: lighten($red, 30);

        &:hover {
          background: $red;
        }
      }
    }

    &.Toastify__toast--warning {
      @extend .is-warning;
    }

    &.Toastify__toast--success {
      @extend .is-success;
    }

    &.Toastify__toast--info {
      @extend .is-info;
    }

    .Toastify__close-button {
      @extend .delete;
    }
  }
}

.error-boundary {
  @extend .notification;
  @extend .is-danger;

  margin: 2em;

  h1 {
    @extend .title;
    @extend .is-4;
    @extend .has-text-danger;
  }

  textarea {
    @extend .textarea;

    margin-top: 1em;
  }

  button {
    @extend .button;
    @extend .is-danger;
  }
}

.markup-input {
  white-space: pre-wrap;
  display: block;

  .ignored {
    color: rgba(0, 0, 0, 0.6);
    padding: 0 0.15em;
  }

  .term {
    color: black;
    background: rgba(0, 0, 0, 0.1);
    border-radius: 3px;
    padding: 0 0.15em;
  }

  .query {
    color: rgb(200, 162, 7);
    padding: 0 0.15em;
  }

  .error {
    color: red;
    background: rgba(255, 0, 0, 0.1);
    border-radius: 3px;
    padding: 0 0.15em;
  }

  .new {
    color: rgb(150, 100, 0);
    padding: 0 0.15em;
  }

  .existing {
    color: rgb(0, 75, 0);
    padding: 0 0.15em;
  }

  .divider {
    color: rgba(0, 0, 0, 0.6);
    padding: 0 0.15em;
  }
}

.image-placeholder {
  background: #ccc;
  display: flex;
  align-items: center;
  justify-content: center;

  svg.fa-star {
    width: 80%;
    height: 80%;
  }

  svg.fa-folder {
    width: 70%;
    height: 70%;
  }

  svg.fa-star,
  svg.fa-folder {
    flex-grow: 0;
    flex-shrink: 0;
    color: #aaa;
  }
}

.categories-selector {
  .categories-selector-row {
    display: flex;

    &:not(:last-child) {
      margin-bottom: 0.75rem;
    }

    .category-selector {
      flex-grow: 1;
    }

    .button {
      margin-left: 10px;
    }
  }
}

.confirm-modal {
  .title {
    margin-top: 1em;
    margin-bottom: 1.5em;
  }
  .level {
    margin-bottom: 0.5em;
  }
}

.react-autosuggest__container {
  .react-autosuggest__input--open {
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
  }

  .react-autosuggest__suggestions-container {
    display: none;
  }

  .react-autosuggest__suggestions-container--open {
    display: block;
    position: absolute;
    top: 100%;
    width: 100%;
    border: 1px solid #aaa;
    background-color: #fff;
    border-bottom-left-radius: 4px;
    border-bottom-right-radius: 4px;
    z-index: 999999;
  }

  .react-autosuggest__suggestions-list {
    margin: 0;
    padding: 0;
    list-style-type: none;
  }

  .react-autosuggest__suggestion {
    cursor: pointer;
    padding: 5px 10px;

    &:not(:first-child) {
      border-top: 1px solid #ccc;
    }
  }

  .react-autosuggest__suggestion--highlighted {
    background-color: #ddd;
  }
}
