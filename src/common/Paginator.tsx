import _ from 'lodash';
import React, { useCallback } from 'react';
import { Pagination } from 'rbx';

export function Paginator({ page, pages, onChangePage }: { page: number, pages: number, onChangePage: (page: number) => unknown}) {
  const changePage = useCallback(_.memoize((newPage: number) => () => onChangePage(newPage)), []);

  let firstPage = 0;
  let lastPage = 0;
  let moreBefore = false;
  let moreAfter = false;
  if (pages <= 5) {
    firstPage = 0;
    lastPage = pages;
  } else if (page - 2 <= 0) {
    firstPage = Math.max(page - 2, 0);
    lastPage = firstPage + 5;
    moreAfter = true;
  } else if (page + 3 >= pages) {
    lastPage = Math.min(page + 3, pages);
    firstPage = lastPage - 5;
    moreBefore = true;
  } else {
    firstPage = page - 2;
    lastPage = page + 3;
    moreBefore = true;
    moreAfter = true;
  }

  return <Pagination rounded>
    <Pagination.Step align="previous" disabled={page === 0} onClick={changePage(page - 1)}>Previous</Pagination.Step>
    { pages > 1 && <Pagination.List>
      { moreBefore && <><Pagination.Link onClick={changePage(0)}>1</Pagination.Link><Pagination.Ellipsis /></> }
      { _.range(firstPage, lastPage).map(eachPage => <Pagination.Link key={eachPage} current={eachPage === page} onClick={changePage(eachPage)}>{eachPage + 1}</Pagination.Link>) }
      { moreAfter && <><Pagination.Ellipsis /><Pagination.Link onClick={changePage(pages - 1)}>{pages}</Pagination.Link></> }
    </Pagination.List> }
    <Pagination.Step align="next" disabled={page === pages - 1} onClick={changePage(page + 1)}>Next</Pagination.Step>
  </Pagination>;
}
