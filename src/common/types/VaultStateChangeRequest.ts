import { VaultState } from './VaultState';

export interface VaultStateChangeRequest {
  state: VaultState,
}
