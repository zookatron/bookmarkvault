import { Bookmark } from './Bookmark';

export interface EditBookmarkResponse {
  bookmark: Bookmark & { categories: string[] },
}
