
export interface RemoteStorageStateChangeRequest {
  address?: string,
  lastSyncTime?: number,
  syncError?: string,
}
