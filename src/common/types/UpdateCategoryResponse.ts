
export interface UpdateCategoryResponse {
  errors?: {
    name?: string,
    image?: string,
    notes?: string,
  },
}
