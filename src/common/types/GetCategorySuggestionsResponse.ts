
export interface GetCategorySuggestionsResponse {
  existingParent: string,
  suggestions: Array<{ name: string, highlightedName: string }>,
}
