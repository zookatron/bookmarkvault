
export interface RemoteStorageCredentials {
  address: string,
  url: string,
  accessToken: string,
}
