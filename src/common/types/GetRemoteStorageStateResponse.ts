
export interface GetRemoteStorageStateResponse {
  address?: string,
  lastSyncTime?: number,
  syncError?: string,
}
