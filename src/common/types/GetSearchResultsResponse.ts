import { HighlightedCategory } from './HighlightedCategory';
import { HighlightedBookmark } from './HighlightedBookmark';

export interface GetSearchResultsResponse {
  categories: HighlightedCategory[],
  bookmarks: HighlightedBookmark[],
  page: number,
  pages: number,
}
