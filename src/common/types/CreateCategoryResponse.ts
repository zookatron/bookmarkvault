
export interface CreateCategoryResponse {
  errors?: {
    name?: string,
    image?: string,
    notes?: string,
  },
}
