import { Bookmark } from './Bookmark';
import { Category } from './Category';
import { Settings } from './Settings';

export interface RemoteStorageDataV1 {
  bookmarks: Array<Bookmark & { categories: string[] }>,
  categories: Category[],
  settings: Settings,
}
