
export interface Settings {
  autoLockEnabled: boolean,
  autoLockTime: number,
}
