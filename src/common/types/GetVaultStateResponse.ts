import { VaultState } from './VaultState';

export interface GetVaultStateResponse {
  state: VaultState,
}
