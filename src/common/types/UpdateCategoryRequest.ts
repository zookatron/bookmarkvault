import { Category } from './Category';

export interface UpdateCategoryRequest {
  name: string,
  changes: Partial<Category>,
}
