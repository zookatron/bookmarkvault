export interface Bookmark {
  url: string,
  name: string,
  image: string | null,
  notes: string,
  created_at: number,
}
