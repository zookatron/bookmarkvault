import { Bookmark } from './Bookmark';

export interface UpdateBookmarkRequest {
  url: string,
  changes: Partial<Bookmark> & { categories?: string[] },
}
