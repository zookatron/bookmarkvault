import { Bookmark } from './Bookmark';
import { Category } from './Category';
import { Settings } from './Settings';
import { Cache } from './Cache';

export interface LocalStorageDataV1 {
  bookmarks: Array<Bookmark & { categories: string[] }>,
  categories: Category[],
  settings: Settings,
  cache: Cache,
}
