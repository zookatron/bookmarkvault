import { Settings } from './Settings';

export interface GetSettingsResponse {
  settings: Settings & { showHidden: boolean },
}
