import { RemoteStorageCredentials } from './RemoteStorageCredentials';

export interface Cache {
  remoteStorage?: {
    credentials: RemoteStorageCredentials,
    version?: string,
    lastSyncTime: number,
    syncError?: string,
  },
  showHidden: boolean,
}
