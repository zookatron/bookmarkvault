
export interface CreateBookmarkResponse {
  errors?: {
    url?: string,
    name?: string,
    image?: string,
    notes?: string,
    categories?: { [categories: string]: string },
  },
}
