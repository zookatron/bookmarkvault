
export interface UpdateRemoteStorageAccountResponse {
  state: 'success' | 'overwrite-remote' | 'choose-version',
}
