import { Bookmark } from './Bookmark';

export interface CreateBookmarkRequest {
  bookmark: Bookmark & { categories: string[] },
}
