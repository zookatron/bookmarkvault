import { Bookmark } from './Bookmark';

export type HighlightedBookmark = Bookmark & { highlighted: Bookmark };
