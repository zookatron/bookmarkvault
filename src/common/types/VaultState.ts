
export type VaultState = 'none' | 'closed' | 'open';
