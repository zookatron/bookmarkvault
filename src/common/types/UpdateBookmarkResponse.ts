
export interface UpdateBookmarkResponse {
  errors?: {
    url?: string,
    name?: string,
    image?: string,
    notes?: string,
    categories?: { [categories: string]: string },
  },
}
