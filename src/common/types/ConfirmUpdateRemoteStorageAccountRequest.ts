
export interface ConfirmUpdateRemoteStorageAccountRequest {
  action: 'cancel' | 'confirm-overwrite-remote' | 'choose-local-version' | 'choose-remote-version',
}
