import { Bookmark } from './Bookmark';

export interface UpdatePopupStateRequest {
  bookmark: Bookmark & { categories: string[] },
}
