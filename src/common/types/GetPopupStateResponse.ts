import { Bookmark } from './Bookmark';

export interface GetPopupStateResponse {
  new?: boolean,
  bookmark?: Bookmark & { categories: string[] },
}
