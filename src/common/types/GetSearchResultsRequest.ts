
export interface GetSearchResultsRequest {
  search: string,
  page?: number,
}
