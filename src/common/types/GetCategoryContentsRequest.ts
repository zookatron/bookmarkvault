
export interface GetCategoryContentsRequest {
  category: string,
  page?: number,
}
