import { Category } from './Category';

export type CreateCategoryRequest = {
  parent: string,
} | {
  category: Category,
};
