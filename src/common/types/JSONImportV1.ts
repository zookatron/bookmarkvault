import { Bookmark } from './Bookmark';
import { Category } from './Category';
import { Settings } from './Settings';

export interface JSONImportV1 {
  version: '1',
  bookmarks: Array<Bookmark & { categories: string[] }>,
  categories: Category[],
  settings: Settings,
}
