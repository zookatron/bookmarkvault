import { Settings } from './Settings';

export type UpdateSettingsRequest = Partial<Settings & { showHidden: boolean }>;
