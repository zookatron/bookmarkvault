import { Category } from './Category';
import { Bookmark } from './Bookmark';

export interface GetCategoryContentsResponse {
  category: Category | null,
  categories: Category[],
  bookmarks: Bookmark[],
  page: number,
  pages: number,
}
