export interface Category {
  name: string,
  image: string | null,
  notes: string,
  created_at: number,
}
