import { Bookmark } from './Bookmark';

export interface PopupStateChangeRequest {
  new?: boolean,
  bookmark?: Bookmark & { categories: string[] },
}
