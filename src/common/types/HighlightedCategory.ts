import { Category } from './Category';

export type HighlightedCategory = Category & { highlighted: Category };
