import React from 'react';
import classnames from 'classnames';

export const Logo = ({ text, size }: { text?: string | JSX.Element, size?: 'large' }) => {
  return <h1 className={ classnames('logo', size === 'large' && 'is-large') }>
    <span className="icon"><img src="img/logo.svg" alt="BookmarkVault" /></span><span className="text">{ text ? text : 'BookmarkVault' }</span>
  </h1>;
};
