import _ from 'lodash';
import { Markup } from './markup';
import { assertNever } from './utils';

interface SearchTerm {
  start: number;
  end: number;
  text: string;
}

export function parseTerms(search: string): SearchTerm[] {
  const terms = [];

  let start = 0;
  let quoted = false;
  for (let char = 0; char < search.length; char++) {
    if (search[char] === ' ' && !quoted) {
      const text = search.slice(start, char);
      if (text !== '' && text !== ' ') {
        terms.push({ start, end: char, text });
      }
      start = char + 1;
    } else if (search[char] === '"' && (char === 0 || search[char - 1] !== '\\')) {
      quoted = !quoted;
    }
  }
  const finalText = search.slice(start);
  if (finalText !== '' && finalText !== ' ') {
    terms.push({ start, end: search.length, text: finalText });
  }

  return terms;
}

interface SearchMarker {
  start: number;
  end: number;
  type: QueryType | 'term' | 'ignored' | 'error';
  text: string;
}

const UnquotedTerm = /^[^":\s]+$/;
const QuotedTerm = /^"([^"\s\\]| |\\"|\\\\)+"$/;
const QueryTerm = /^([^":\s]+):((?:[^":\s]+)|("(?:[^"\s\\]| |\\"|\\\\)+"))?$/;
const ReservedTerms = ['the', 'and', 'not', 'for', 'that', 'have', 'with', 'url', 'name', 'notes', 'cat', 'no-cat'];
type QueryType = 'url' | 'name' | 'notes' | 'cat';
const QueryTerms: QueryType[] = ['url', 'name', 'notes', 'cat'];
const isQueryTerm = (term: string): term is QueryType => _.includes(QueryTerms, term);

export function parseQuotedText(text: string) {
  return text.slice(1, text.length - 1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
}

export function parseSearch(search: string): SearchMarker[] {
  const terms: SearchMarker[] = [];

  for (const term of parseTerms(search)) {
    if (term.text === '') {
      continue;
    } else if (term.text.match(UnquotedTerm)) {
      if (term.text.length <= 2 || _.includes(ReservedTerms, term.text)) {
        terms.push({ start: term.start, end: term.end, type: 'ignored', text: term.text });
      } else {
        terms.push({ start: term.start, end: term.end, type: 'term', text: term.text });
      }
    } else if (term.text.match(QuotedTerm)) {
      terms.push({ start: term.start, end: term.end, type: 'term', text: parseQuotedText(term.text) });
    } else if (term.text.match(QueryTerm)) {
      const matches = term.text.match(QueryTerm) as RegExpMatchArray;
      const query = matches[1];
      if (isQueryTerm(query)) {
        let text = matches.length >= 3 ? matches[2] || '' : '';
        if (text.match(QuotedTerm)) { text = parseQuotedText(text); }
        terms.push({ start: term.start, end: term.end, type: query, text });
      } else {
        terms.push({ start: term.start, end: term.end, type: 'error', text: term.text });
      }
    } else {
      terms.push({ start: term.start, end: term.end, type: 'error', text: term.text });
    }
  }
  return terms;
}

export function getSearchMarkup(search: string): Markup[] {
  const markup: Markup[] = [];

  for (const term of parseSearch(search)) {
    if (term.type === 'ignored') {
      markup.push({ start: term.start, end: term.end, class: 'ignored' });
    } else if (term.type === 'term') {
      markup.push({ start: term.start, end: term.end, class: 'term' });
    } else if (isQueryTerm(term.type)) {
      markup.push({ start: term.start, end: term.start + term.type.length + 1, class: 'query' });
      markup.push({ start: term.start + term.type.length + 1, end: term.end, class: 'term' });
    } else if (term.type === 'error') {
      markup.push({ start: term.start, end: term.end, class: 'error' });
    } else {
      assertNever(term.type);
    }
  }

  return markup;
}
