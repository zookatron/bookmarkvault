import _ from 'lodash';
import React, { useCallback, useState, useMemo } from 'react';
import { Field, Control, Label, Select, Textarea, Input } from 'rbx';
import classnames from 'classnames';

type FormFieldSize = 'small' | 'medium' | 'large';
type FormFieldType = 'select' | 'textarea' | 'number' | 'time' | 'text' | 'color' | 'email' | 'tel' | 'password' | 'search' | 'date';
export interface FormFieldInputProps {
  id: string,
  size?: FormFieldSize,
  color?: 'danger',
  value: string,
  disabled?: boolean,
  placeholder?: string,
  onChange: (event: { target: { value: string } }) => void,
  onFocus: () => void,
  onBlur: () => void,
}

export function FormField({ className, input, size, type, label, value, error, disabled, placeholder, options, emptyOption, onChange, onFocus, onBlur }:
  { className?: string, input?: (props: FormFieldInputProps) => React.ReactNode, size?: FormFieldSize, type?: FormFieldType, value: string,
    label?: string, error?: string | boolean, disabled?: boolean, placeholder?: string, options?: Array<{label: string, value: string}>,
    emptyOption?: boolean, onChange?: (value: string) => unknown, onFocus?: () => unknown, onBlur?: () => unknown }) {
  const [focused, setFocused] = useState(false);
  const id = useMemo(() => _.uniqueId('form_field_'), []);
  const finalOnChange = useCallback((event: { target: { value: string } }) => { if (onChange) { onChange(event.target.value); } }, [onChange]);
  const finalOnFocus = useCallback(() => { setFocused(true); if (onFocus) { onFocus(); } }, [setFocused, onFocus]);
  const finalOnBlur = useCallback(() => { setFocused(false); if (onBlur) { onBlur(); } }, [setFocused, onBlur]);

  const inputProps: FormFieldInputProps = {
    id,
    size,
    color: error ? 'danger' : undefined,
    value,
    disabled,
    placeholder,
    onChange: finalOnChange,
    onFocus: finalOnFocus,
    onBlur: finalOnBlur,
  };

  let inputElement = null;
  if (input) {
    inputElement = input(inputProps);
  } else if (type === 'select') {
    inputElement = <Select {...inputProps}>
      { emptyOption === false ? null : <Select.Option value="">{ _.isString(emptyOption) ? emptyOption : '-- Choose One --' }</Select.Option> }
      { (options || []).map(option => <Select.Option key={ option.value } value={ option.value }>{ option.label }</Select.Option>) }
    </Select>;
  } else if (type === 'textarea') {
    inputElement = <Textarea {...inputProps} />;
  } else {
    inputElement = <Input type={ type || 'text' } {...inputProps} />;
  }

  const hasError = error && _.isString(error);
  inputElement = <div className={ classnames(hasError && 'has-tooltip-bottom has-tooltip-danger', hasError && focused && 'has-tooltip-active') } data-tooltip={error}>
    {inputElement}
  </div>;

  return label ? <Field horizontal className={className}>
    <Field.Label size={size || 'normal'}>
      <Label htmlFor={id}>{label}:</Label>
    </Field.Label>
    <Field.Body>
      <Field>
        <Control>{inputElement}</Control>
      </Field>
    </Field.Body>
  </Field> : <Field className={className}>
    <Control>{inputElement}</Control>
  </Field>;
}
