import _ from 'lodash';
import browser, { Tabs } from 'webextension-polyfill';
import { validate, validationErrors, assertNever } from './utils';
import { EmptyRequest } from './types/EmptyRequest';
import { EmptyResponse } from './types/EmptyResponse';

// ------------------------------------------
// Misc
// ------------------------------------------

export function getVersion() {
  return browser.runtime.getManifest().version;
}

export async function setPageAction(action: 'tab' | 'popup') {
  if (action === 'tab') {
    return browser.browserAction.setPopup({ popup: '' });
  } else if (action === 'popup') {
    return browser.browserAction.setPopup({ popup: 'popup.html' });
  } else {
    return assertNever(action);
  }
}
browser.browserAction.onClicked.addListener(openTab);

export async function storageGet<Key extends string>(keys: Key[]): Promise<{ [key in Key]: unknown }> {
  return browser.storage.local.get(keys) as Promise<{ [key in Key]: unknown }>;
}

export async function storageSet(items: { [key: string]: unknown }): Promise<void> {
  return browser.storage.local.set(items);
}

export async function storageRemove(keys: string[]): Promise<void> {
  return browser.storage.local.remove(keys);
}

export async function storageClear(): Promise<void> {
  return browser.storage.local.clear();
}

export function getOAuthRedirectURL() {
  return browser.identity.getRedirectURL();
}

export async function getOAuthResponse(url: string) {
  return browser.identity.launchWebAuthFlow({ interactive: true, url });
}

// ------------------------------------------
// Tabs
// ------------------------------------------

let currentTabId: Promise<number> | null = null;
export async function openTab() {
  if (currentTabId === null) {
    currentTabId = (async () => {
      const tab = await browser.tabs.create({ url: browser.runtime.getURL('tab.html'), active: true });
      if (_.isNil(tab.id)) { throw new Error('Unable to read new tab ID'); }
      return tab.id;
    })();
    await currentTabId;
  } else {
    const tabId = await currentTabId;
    const tab = await browser.tabs.get(tabId);
    if (!_.isNil(tab.windowId)) { await browser.windows.update(tab.windowId, { focused: true }); }
    browser.tabs.update(tabId, { active: true });
  }
}

browser.tabs.onRemoved.addListener(async removedTabId => {
  if (currentTabId) {
    const tabId = await currentTabId;
    if (tabId === removedTabId) { currentTabId = null; }
  }
});

// Apparently the browserAction.onClicked event is not fired if a popup is set
// on the browserAction config, which means the only way to detect the click in
// this case is to be notified by the popup when it opens.
const popupOpenedListeners: Array<(tab?: Tabs.Tab) => unknown> = [];
export async function popupOpened() {
  const tabs = await browser.tabs.query({ active: true, currentWindow: true });
  const tab = _.first(tabs);
  for (const listener of popupOpenedListeners) {
    listener(tab);
  }
}

export function onActiveTabChanged(listener: (tab?: Tabs.Tab, lostAccess?: boolean) => unknown) {
  let activeTabId: number | undefined;
  let previousTabData: { url?: string, title?: string } = {};

  const changeTab = (tab?: Tabs.Tab) => {
    if (tab) {
      if (tab.id !== activeTabId || !_.isEqual(previousTabData, { url: tab.url, title: tab.title })) {
        const lostAccess = tab.id === activeTabId && previousTabData.url !== undefined && tab.url === undefined;
        activeTabId = tab.id;
        previousTabData = { url: tab.url, title: tab.title };
        listener(tab, lostAccess);
      }
    } else {
      if (activeTabId !== undefined) {
        activeTabId = undefined;
        previousTabData = { url: undefined, title: undefined };
        listener(undefined);
      }
    }
  };

  popupOpenedListeners.push(changeTab);
  browser.tabs.onUpdated.addListener((tabId, changeInfo, tab) => tabId === activeTabId && changeTab(tab));
}

// ------------------------------------------
// Messaging
// ------------------------------------------

interface MessageHandler { name: string, requestSchema: object, handler: (message: unknown) => unknown }
const messageHandlers: MessageHandler[] = [];

export function registerMessageHandler<Request extends EmptyRequest, Response extends EmptyResponse | void>(name: string, requestSchema: object, handler: (message: Request) => Response | Promise<Response>) {
  messageHandlers.push({ name, requestSchema, handler: handler as MessageHandler['handler'] });
}

// tslint:disable-next-line:no-any
function handleMessage(message: any): Promise<any> | undefined {
  if (!_.isPlainObject(message) || !_.isString(message.type) || !_.isPlainObject(message.request)) { console.debug(`Ignoring invalid request ${JSON.stringify(message)}`); return; }
  const messageHandler = _.find(messageHandlers, { name: message.type as string });
  if (!messageHandler) { console.debug(`Ignoring request type "${message.type}" with no handler`); return; }
  if (!validate(message.request, messageHandler.requestSchema)) {
    return Promise.resolve({ error: `Invalid request object recieved: ${validationErrors(message.request, messageHandler.requestSchema).join(', ')}` });
  }
  return Promise.resolve(messageHandler.handler(message.request)).then(response => _.isNil(response) ? {} : response, error => ({ error: error.message }));
}

browser.runtime.onMessage.addListener(handleMessage);

export async function sendMessageNoResponse<Request extends EmptyRequest>(type: string, request: Request | undefined) {
  browser.runtime.sendMessage({ type, request: _.isNil(request) ? {} : request })
    .then(response => console.debug(`Ignoring sendMessage response ${JSON.stringify(response)}`), error => console.debug(`Ignoring sendMessage error ${error}`));
}

export async function sendMessage<Request extends EmptyRequest, Response extends EmptyResponse>(type: string, request: Request | undefined, responseSchema: object): Promise<Response> {
  const response = await browser.runtime.sendMessage({ type, request: _.isNil(request) ? {} : request });
  if (_.isPlainObject(response) && _.isString(response.error)) { throw new Error(response.error); }
  if (!validate<Response>(response, responseSchema)) {
    throw new Error(`Invalid response object recieved: ${validationErrors(response, responseSchema).join(', ')}`);
  }
  return response;
}
