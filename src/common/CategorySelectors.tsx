import _ from 'lodash';
import classnames from 'classnames';
import React, { useCallback, useMemo, useState, useEffect } from 'react';
import Autosuggest, { RenderInputComponentProps } from 'react-autosuggest';
import { Button, Icon } from 'rbx';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faMinus } from '@fortawesome/free-solid-svg-icons';
import { HTML } from './HTML';
import { FormField, FormFieldInputProps } from './FormField';
import { Markup, MarkupInput } from './markup';
import { validateCategoryName } from './utils';
import { GetCategorySuggestionsRequest } from './types/GetCategorySuggestionsRequest';
import { GetCategorySuggestionsResponse } from './types/GetCategorySuggestionsResponse';

function getCategoryMarkup(category: string, existingParent?: string) {
  if (!category) { return []; }
  const parts = category.split('/');
  const markups: Markup[] = [];
  let start = 0;
  parts.slice(0, -1).forEach(part => {
    markups.push({ start, end: start + part.length, class: 'new' });
    markups.push({ start: start + part.length, end: start + part.length + 1, class: 'divider' });
    start += part.length + 1;
  });
  markups.push({ start, end: start + parts[parts.length - 1].length, class: 'new' });
  if (!_.isNil(existingParent)) {
    const existingParts = existingParent.split('/');
    _.zip(_.filter(markups, { class: 'new' }), existingParts).forEach(([markup, existingPart]) => {
      if (markup && existingPart && category.slice(markup.start, markup.end) === existingPart) {
        markup.class = 'existing';
      }
    });
  }
  for (let markupIndex = markups.length - 1; markupIndex >= 0; markupIndex -= 1) {
    const markup = markups[markupIndex];
    if (markup.class === 'divider' || markup.end - markup.start !== 0) { continue; }
    const startMarkup = markupIndex === 0 ? markupIndex : markupIndex - 1;
    const endMarkup = markupIndex === markups.length - 1 ? markupIndex : markupIndex + 1;
    markups.splice(startMarkup, endMarkup - startMarkup + 1, { start: markups[startMarkup].start, end: markups[endMarkup].end, class: 'error' });
    markupIndex -= 1;
  }
  return _.isNil(existingParent) ? _.filter(markups, markup => markup.class !== 'new') : markups;
}

function getCategoryErrors(category: string) {
  if (category && !validateCategoryName(category)) { return 'Invalid category name'; }
}

export function CategorySelector({ className, size, label, disabled, placeholder, category, errors, onChange, getCategorySuggestions }:
  { className?: string, size?: 'small' | 'medium' | 'large', label?: string, disabled?: boolean, placeholder?: string,
    category: string, errors: unknown, onChange: (category: string | undefined, errors: unknown) => unknown,
    getCategorySuggestions: (request: GetCategorySuggestionsRequest) => Promise<GetCategorySuggestionsResponse> }) {
  const [suggestions, setSuggestions] = useState([] as Array<{ name: string, highlightedName: string }>);
  const [existingParent, setExistingParent] = useState('');
  const onSuggestionsFetchRequested = useCallback(async ({ value }: { value: string }) => {
    try {
      const response = await getCategorySuggestions({ category: value });
      setSuggestions(response.suggestions);
      setExistingParent(response.existingParent);
    } catch (error) {
      setSuggestions([]);
      onChange(undefined, (error as Error).message);
    }
  }, [getCategorySuggestions, onChange, setSuggestions, setExistingParent]);
  useEffect(() => void onSuggestionsFetchRequested({ value: category }), []);
  const onSuggestionsClearRequested = useCallback(() => setSuggestions([]), [setSuggestions]);
  const onSuggestionSelected = useCallback((event, suggestion) => setExistingParent(suggestion.suggestion.name), [setExistingParent]);
  const onInputChange = useCallback((newCategory: string) => onChange(newCategory, getCategoryErrors(newCategory)), [onChange]);
  const unwrapOnChange = useCallback(_.memoize((formFieldOnChange: (event: { target: { value: string } }) => unknown) => {
    return (event: unknown, { newValue: value }: { newValue: string }) => formFieldOnChange({ target: { value } });
  }), []);
  const wrapOnChange = useCallback(_.memoize((autosuggestOnChange: React.ChangeEventHandler<HTMLDivElement>) => (newValue: string) => {
    return autosuggestOnChange({ target: { value: newValue } } as unknown as React.ChangeEvent<HTMLDivElement>);
  }), []);
  const innerInput = useCallback((inputProps: React.InputHTMLAttributes<HTMLDivElement>) => <MarkupInput
    { ...inputProps }
    value={inputProps.value as string}
    color={inputProps.color as 'danger' | undefined}
    onChange={wrapOnChange(inputProps.onChange!)}
    markup={getCategoryMarkup(inputProps.value as string, existingParent)}
  />, [existingParent]);
  const getSuggestionValue = useCallback((suggestion: { name: string, highlightedName: string }) => suggestion.name, []);
  const renderSuggestion = useCallback((suggestion: { name: string, highlightedName: string }) => <HTML html={suggestion.highlightedName}/>, []);
  const input = useCallback((inputProps: FormFieldInputProps) => <Autosuggest
    suggestions={suggestions}
    onSuggestionsFetchRequested={onSuggestionsFetchRequested}
    onSuggestionsClearRequested={onSuggestionsClearRequested}
    onSuggestionSelected={onSuggestionSelected}
    getSuggestionValue={getSuggestionValue}
    renderSuggestion={renderSuggestion}
    renderInputComponent={innerInput}
    inputProps={ _.assign(inputProps, { onChange: unwrapOnChange(inputProps.onChange), size: undefined }) }
  />, [suggestions, onSuggestionsFetchRequested, onSuggestionsClearRequested, innerInput]);
  return <FormField
    className={classnames('category-selector', className)}
    size={size}
    label={label}
    error={_.isString(errors) ? errors : undefined}
    disabled={disabled}
    placeholder={placeholder}
    value={category}
    input={input}
    onChange={onInputChange}
  />;
}

function getCategoriesErrors(categories: string[], errors: unknown) {
  const finalCategories = categories.length ? categories : [''];
  const errorsArray = _.isArray(errors) ? errors : [];
  const finalErrors = finalCategories.map((category, index) => {
    const categoryErrors = errorsArray[index] || {};
    const categoriesError = finalCategories.length > 1 && category === '' ? 'You must enter a category name' : undefined;
    const categoryError = categoryErrors.category;
    return categoriesError || categoryError ? { categories: categoriesError, category: categoryError } : undefined;
  });
  return _.filter(finalErrors).length ? finalErrors : undefined;
}

export function CategoriesSelector({ className, size, label, disabled, placeholder, categories, errors, onChange, getCategorySuggestions }:
  { className?: string, size?: 'small' | 'medium' | 'large', label?: string, disabled?: boolean, placeholder?: string,
    categories: string[], errors: unknown, onChange: (categories: string[], errors: unknown) => unknown,
    getCategorySuggestions: (request: GetCategorySuggestionsRequest) => Promise<GetCategorySuggestionsResponse> }) {
  const addCategory = useCallback(_.memoize((index: number) => () => {
    const newCategories = categories.length ? _.clone(categories) : [''];
    newCategories.splice(index + 1, 0, '');
    onChange(newCategories, getCategoriesErrors(newCategories, errors));
  }), [categories, errors, onChange]);
  const removeCategory = useCallback(_.memoize((index: number) => () => {
    const newCategories = categories.length ? _.clone(categories) : [''];
    newCategories.splice(index, 1);
    onChange(newCategories, getCategoriesErrors(newCategories, errors));
  }), [categories, errors]);
  const changeCategory = useCallback(_.memoize((index: number) => (category: string | undefined, categoryErrors: unknown) => {
    const newCategories = categories.length ? _.clone(categories) : [''];
    if (!_.isNil(category)) { newCategories[index] = category; }
    const newErrors = _.isArray(errors) ? _.clone(errors) : [];
    newErrors[index] = newErrors[index] || {};
    newErrors[index].category = categoryErrors;
    onChange(newCategories, getCategoriesErrors(newCategories, newErrors));
  }), [categories, errors]);
  const finalCategories = useMemo(() => categories.length ? categories : [''], [categories]);
  const finalErrors = useMemo(() => _.isArray(errors) ? errors : [], [errors]);
  const input = useCallback(() => <div className="categories-selector">
    { finalCategories.map((category, index) => <div key={index} className="categories-selector-row">
        <CategorySelector
          disabled={disabled}
          placeholder={placeholder}
          category={category}
          errors={finalErrors[index] ? finalErrors[index].category || finalErrors[index].categories : undefined}
          onChange={changeCategory(index)}
          getCategorySuggestions={getCategorySuggestions}
        />
        <Button onClick={removeCategory(index)} disabled={finalCategories.length === 1 && category === ''} color="dark"><Icon><FontAwesomeIcon icon={faMinus} /></Icon></Button>
        <Button onClick={addCategory(index)} color="dark"><Icon><FontAwesomeIcon icon={faPlus} /></Icon></Button>
      </div>) }
  </div>, [finalCategories, finalErrors, disabled, placeholder, addCategory, removeCategory, changeCategory, getCategorySuggestions]);
  return <FormField className={classnames('category-selector', className)} size={size} label={label} input={input} value="" />;
}
