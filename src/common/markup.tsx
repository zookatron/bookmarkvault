import _ from 'lodash';
import React, { useCallback, useState } from 'react';
import { htmlDecode, htmlEncode, removeChars } from './utils';

export interface Markup {
  start: number;
  end: number;
  class: string;
}

export function applyMarkup(value: string, markup: Markup[]): string {
  const normalizedMarkers = _.sortBy(_.cloneDeep(markup), ['start', 'end']);

  let foundOverlap = true;
  while (foundOverlap) {
    foundOverlap = false;
    for (let marker = 0; marker < normalizedMarkers.length; marker++) {
      for (let nextmarker = marker + 1; nextmarker < normalizedMarkers.length; nextmarker++) {
        if (normalizedMarkers[marker].start > normalizedMarkers[nextmarker].start) { throw new Error('Invalid applyMarkup state: Improper start sorting'); }
        if (normalizedMarkers[marker].end > normalizedMarkers[nextmarker].start) {
          if (normalizedMarkers[marker].class !== normalizedMarkers[nextmarker].class) {
            throw new Error(`Markup has overlapping classes "${normalizedMarkers[marker].class}" and "${normalizedMarkers[nextmarker].class}"!`);
          }
          normalizedMarkers[marker].end = Math.max(normalizedMarkers[marker].end, normalizedMarkers[nextmarker].end);
          normalizedMarkers.splice(nextmarker, 1);
          foundOverlap = true;
          break;
        }
        if (normalizedMarkers[marker].end > normalizedMarkers[nextmarker].end) { throw new Error('Invalid applyMarkup state: Improper end sorting'); }
      }
      if (foundOverlap) { break; }
    }
  }

  let output = '';
  let position = 0;
  for (const marker of normalizedMarkers) {
    output += htmlEncode(value.slice(position, marker.start));
    output += `<span class="${marker.class}">`;
    output += htmlEncode(value.slice(marker.start, marker.end));
    output += '</span>';
    position = marker.end;
  }
  output += htmlEncode(value.slice(position));
  return output;
}

// tslint:disable:align
export const MarkupInput = React.forwardRef(({ value, className, tabIndex, color, markup, onChange, onFocus, onBlur, onKeyDown, onKeyUp }:
  { value: string, className?: string, tabIndex?: number, color?: 'danger' | undefined, markup: Markup[], onChange: (value: string) => unknown,
    onFocus?: (event: React.FocusEvent<HTMLDivElement>) => unknown, onBlur?: (event: React.FocusEvent<HTMLDivElement>) => unknown,
    onKeyUp?: (event: React.KeyboardEvent<HTMLDivElement>) => unknown, onKeyDown?: (event: React.KeyboardEvent<HTMLDivElement>) => unknown }, ref: React.Ref<HTMLDivElement>) => {
  const [version, setVersion] = useState(0);
  const onContentChange = useCallback(changeValue => {
    onChange(removeChars(htmlDecode(changeValue.replace(/<(?:.|\n)*?>/gm, '')), ['\n', '\r']));
    // Version is used to refresh markup after each HTML change, ensuring filtered characters like newlines are removed
    setVersion(version => version + 1);
  }, [setVersion]);
  return <ContentEditable
    ref={ref}
    className={`${className ? className : ''} ${color === 'danger' ? 'is-danger' : ''} input markup-input`}
    tabIndex={tabIndex}
    spellCheck={false}
    html={applyMarkup(value, markup)}
    onChange={onContentChange}
    onFocus={onFocus}
    onBlur={onBlur}
    onKeyUp={onKeyUp}
    onKeyDown={onKeyDown}
    version={version}
  />;
});
// tslint:enable:align

/* ---------------------------------------------------- */
/* ContentEditable (react-contenteditable)              */
/* ---------------------------------------------------- */

function getTextNodes(node: Node): Node[] {
  if (node.nodeType === Node.TEXT_NODE) { return [node]; }

  let nodes: Node[] = [];
  for (const childNode of Array.from(node.childNodes)) {
    nodes = nodes.concat(getTextNodes(childNode));
  }
  return nodes;
}

interface ContentEditableProps {
  html: string,
  onChange?: (value: string) => unknown,
  onBlur?: (event: React.FocusEvent<HTMLDivElement>) => unknown,
  onFocus?: (event: React.FocusEvent<HTMLDivElement>) => unknown,
  onKeyUp?: (event: React.KeyboardEvent<HTMLDivElement>) => unknown,
  onKeyDown?: (event: React.KeyboardEvent<HTMLDivElement>) => unknown,
  disabled?: boolean,
  className?: string,
  tabIndex?: number,
  spellCheck?: boolean,
  version?: number,
}

class ContentEditableBase extends React.Component<ContentEditableProps & { forwardedRef: React.Ref<HTMLDivElement> }> {
  private lastHtml: string = this.props.html;
  private element: HTMLDivElement | null = null;
  private resetCursor = false;

  constructor(props: ContentEditableProps & { forwardedRef: React.Ref<HTMLDivElement> }) {
    super(props);
    this.updateElement = this.updateElement.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.onFocus = this.onFocus.bind(this);
    this.onKeyUp = this.onKeyUp.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);
  }

  render() {
    const { disabled, className, tabIndex, spellCheck } = this.props;

    return <div
      ref={this.updateElement}
      onInput={this.emitChange}
      onBlur={this.onBlur}
      onFocus={this.onFocus}
      onKeyUp={this.onKeyUp}
      onKeyDown={this.onKeyDown}
      contentEditable={!disabled}
      className={className}
      spellCheck={spellCheck}
      data-gramm="false"
      { ...{ tabIndex } }
    />;
  }

  withElement(operation: (element: HTMLDivElement) => void) {
    if (this.element) {
      operation(this.element);
    }
  }

  updateElement(element: HTMLDivElement | null) {
    this.element = element;
    if (this.props.forwardedRef) {
      if (typeof this.props.forwardedRef === 'function') {
        this.props.forwardedRef(element);
      } else {
        (this.props.forwardedRef as unknown as { current: HTMLDivElement | null }).current = element;
      }
    }
  }

  onBlur(event: React.FocusEvent<HTMLDivElement>) {
    this.emitChange(event);
    if (this.props.onBlur) { this.props.onBlur(event); }
  }

  onFocus(event: React.FocusEvent<HTMLDivElement>) {
    if (this.props.onFocus) { this.props.onFocus(event); }
  }

  onKeyUp(event: React.KeyboardEvent<HTMLDivElement>) {
    this.emitChange(event);
    if (this.props.onKeyUp) { this.props.onKeyUp(event); }
  }

  onKeyDown(event: React.KeyboardEvent<HTMLDivElement>) {
    if (event.keyCode === 13) {
      this.resetCursor = true;
    }
    this.emitChange(event);
    if (this.props.onKeyDown) { this.props.onKeyDown(event); }
  }

  shouldComponentUpdate(nextProps: ContentEditableProps): boolean {
    const { props } = this;
    return props.html !== nextProps.html ||
      props.disabled !== nextProps.disabled ||
      props.className !== nextProps.className ||
      props.spellCheck !== nextProps.spellCheck ||
      props.version !== nextProps.version;
  }

  componentDidMount() {
    this.withElement(element => {
      element.innerHTML = this.props.html;
    });
  }

  componentDidUpdate() {
    this.withElement(element => {
      const selection = window.getSelection();
      let previousCursor: number | null = null;
      if (selection && selection.focusNode && document.activeElement === element) {
        previousCursor = 0;
        const nodes = selection.focusNode.nodeType === Node.TEXT_NODE ? getTextNodes(element) : Array.from(element.childNodes);
        for (const node of nodes) {
          if (node === selection.focusNode) {
            previousCursor += selection.focusOffset;
            break;
          } else {
            previousCursor += _.sumBy(getTextNodes(node), textNode => textNode.nodeValue ? textNode.nodeValue.length : 0);
          }
        }
      }

      if (this.props.html !== element.innerHTML) {
        element.innerHTML = this.lastHtml = this.props.html;
      }

      if (selection && previousCursor !== null) {
        let findCursor = 0;
        let newFocus: Node | null = null;
        let newCursor: number | null = null;
        const nodes = getTextNodes(element);
        if (!this.resetCursor) {
          for (const node of nodes) {
            const length = node.nodeValue ? node.nodeValue.length : 0;
            if (findCursor + length >= previousCursor) {
              newFocus = node;
              newCursor = previousCursor - findCursor;
              break;
            } else {
              findCursor += length;
            }
          }
        }
        this.resetCursor = false;
        if (newFocus === null) { newFocus = _.last(nodes) || null; }
        if (newFocus === null) { newFocus = element; }
        if (newCursor === null) { newCursor = newFocus.nodeValue ? newFocus.nodeValue.length : 0; }

        const range = document.createRange();
        range.setStart(newFocus, newCursor);
        range.collapse(true);
        selection.removeAllRanges();
        selection.addRange(range);
      }
    });
  }

  emitChange = (originalEvt: React.SyntheticEvent<HTMLDivElement>) => {
    this.withElement(element => {
      const html = element.innerHTML;
      if (this.props.onChange && html !== this.lastHtml) {
        this.props.onChange(html);
      }
      this.lastHtml = html;
    });
  }
}

const ContentEditable = React.forwardRef((props: ContentEditableProps, ref: React.Ref<HTMLDivElement>) => {
  return <ContentEditableBase {...props} forwardedRef={ref} />;
});
