import _ from 'lodash';
import { validate as validateJSON } from 'jsonschema';
import React, { useContext } from 'react';
import { configure } from 'mobx';
import { toast } from 'react-toastify';
import { Base64 } from 'js-base64';

export function validate<T>(value: unknown, schema: object): value is T {
  if (!value) { return false; }
  const result = validateJSON(value, schema);
  return result.errors.length === 0;
}

export function validationErrors<T>(value: unknown, schema: object) {
  if (!value) { return ['No value provided!']; }
  const result = validateJSON(value, schema);
  return result.errors;
}

export function htmlDecode(input: string) {
  return _.unescape(input.replace(/&nbsp;|\u202F|\u00A0/g, ' '));
}

export function htmlEncode(input: string) {
  return _.escape(input);
}

export function delay(time = 0) {
  return new Promise<void>(resolve => setTimeout(resolve, time));
}

export function binPack<T>(items: T[], itemSize: (item: T) => number, binSize: number): T[][] {
  const bins = [];
  let currentBin: T[] = [];
  let currentBinSize = 0;
  for (const item of items) {
    const size = itemSize(item);
    if (currentBinSize + size > binSize) {
      bins.push(currentBin);
      currentBin = [item];
      currentBinSize = size;
    } else {
      currentBin.push(item);
      currentBinSize += size;
    }
  }
  bins.push(currentBin);
  return bins;
}

export function readFile(file: File) {
  return new Promise<string>((resolve, reject) => {
    const reader = new FileReader();
    reader.addEventListener('loadend', () => reader.error ? reject(reader.error) : resolve(reader.result as string));
    reader.readAsText(file);
  });
}

export function downloadFile(name: string, mime: string, contents: string) {
  const link = document.createElement('a');
  link.download = name;
  link.href = `data:${mime};charset=utf-8;base64,${Base64.encode(contents)}`;
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
}

export async function withRollback<T>(operation: (addRollback: (rollback: () => unknown) => void) => Promise<T>): Promise<T> {
  const rollbacks: Array<() => unknown> = [];
  const addRollback = (rollback: () => unknown) => rollbacks.push(rollback);
  try {
    return await operation(addRollback);
  } catch (error) {
    try {
      for (let index = rollbacks.length - 1; index >= 0; index--) {
        await rollbacks[index]();
      }
    } catch (rollbackError) {
      throw new Error(`Encountered error while attempting to rollback operation: ${(rollbackError as Error).message} (Original Error: ${(error as Error).message})`);
    }
    throw error;
  }
}

export function throwError(error: Error | string) {
  if (typeof error === 'string') {
    throw new Error(error);
  } else {
    throw error;
  }
}

export function makeWithState<StateType>(state: StateType): <PropType extends { state: StateType }>(WrappedComponent: React.FunctionComponent<PropType>) => React.FunctionComponent<Omit<PropType, 'state'>> {
  configure({ computedRequiresReaction: true, enforceActions: 'observed' });
  const storeContext = React.createContext(state);
  return <PropType extends { state: StateType }>(WrappedComponent: React.FunctionComponent<PropType>): React.FunctionComponent<Omit<PropType, 'state'>> => {
    return (props: Omit<PropType, 'state'>) => <WrappedComponent {...props as PropType} state={useContext(storeContext)} />;
  };
}

export function displayErrors<Arguments extends unknown[], Result>(operation: (...args: Arguments) => Result, rethrow?: boolean): (...args: Arguments) => Promise<void> {
  return async (...args: Arguments) => {
    try {
      await operation(...args);
    } catch (error) {
      toast.error((error as Error).message);
      if (rethrow) { throw error; }
    }
  };
}

export function preventDefault<T>(wrappedFunction: () => T): (event: React.SyntheticEvent<unknown>) => T {
  return (event: React.SyntheticEvent<unknown>) => {
    event.stopPropagation();
    event.preventDefault();
    return wrappedFunction();
  };
}

export type Resolved<T> = T extends Promise<infer R> ? R : never;

export type Omit<T, K> = Pick<T, Exclude<keyof T, K>>;

// tslint:disable-next-line:no-any
export interface AnyObject { [key: string]: any }

export function assertNever(value: never): never {
  throw new Error(`Unexpected value ${value}`);
}

const CategoryRegex = /^[^\/\s]([^\/]*[^\/\s])?(\/[^\/\s]([^\/]*[^\/\s])?)*$/;
export function validateCategoryName(category: string) {
  if (!category) { return false; }
  return !!category.match(CategoryRegex);
}

export function getCategoryLastName(category: string) {
  return (_.last(category.replace(/(<)(\/)([^>]*>)/g, '$1\\$3').split('/')) || '').replace(/(<)(\\)([^>]*>)/g, '$1/$3');
}

export function getCategoryParent(category: string) {
  return category.split('/').slice(0, -1).join('/');
}

export function getCategoryParents(category: string) {
  const parts = category.split('/');
  const parents = _.range(1, parts.length).map(fromEnd => parts.slice(0, -fromEnd).join('/'));
  return parents;
}

export function matchAll(str: string, regex: RegExp) {
  let match;
  const matches = [];
  // tslint:disable-next-line:no-conditional-assignment
  while ((match = regex.exec(str)) != null) { matches.push(match); }
  return matches;
}

export function removeChars(from: string, chars: string[]) {
  const fromChars = [...from];
  for (let fromChar = fromChars.length - 1; fromChar >= 0; fromChar--) {
    if (_.includes(chars, fromChars[fromChar])) {
      fromChars.splice(fromChar, 1);
    }
  }
  return fromChars.join('');
}
