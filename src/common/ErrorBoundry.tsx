import React, { PureComponent } from 'react';

export class ErrorBoundry extends PureComponent<{}, { error: Error | false }> {
  constructor(props: {}) {
    super(props);
    this.state = { error: false };
  }

  static getDerivedStateFromError(error: Error) {
    return { error };
  }

  reload() {
    window.location.reload();
  }

  render() {
    if (this.state.error) {
      return <div className="error-boundary">
        <h1>Uh oh! Something went wrong!</h1>
        <p>The extension has encountered an error it was not able to recover from. Please send the following
        data to the developers along with a description of what you were doing at the time of the error:</p>
        <textarea rows={ 10 } cols={ 100 } value={ `${this.state.error.message}\n${this.state.error.stack}` } readOnly={ true } /><br />
        <button onClick={ this.reload }>Reload Page</button>
      </div>;
    } else {
      return this.props.children;
    }
  }
}
