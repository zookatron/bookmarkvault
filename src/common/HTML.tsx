import React from 'react';

export function HTML({ html }: { html: string }) {
  return <span dangerouslySetInnerHTML={{ __html: html }} />;
}
