declare module '*/*.scss' {
  const styles: { [key: string]: string };
  export default styles;
}

declare module 'react-file-picker' {
  class FilePicker extends React.Component<{
    children: React.Node,
    onChange: (file: File) => any,
    onError: (error: string) => any,
    maxSize?: number,
    extensions?: string[],
    style?: React.CSSProperties,
  }> {};
}
