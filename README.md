
# BookmarkVault

BookmarkVault is a browser extension that lets you easily and securely store your bookmarks. It stores your bookmarks using AES encryption, locked by a passphrase you set when creating the vault. The passphrase is never written to disk anywhere, and only lives in system memory while the plugin is unlocked for maximum security. BookmarkVault can also save your bookmark data to a [RemoteStorage](https://remotestorage.io/) account for easy and secure synchronization between browsers.

## Build Instructions

#### Using Docker

Docker is the easiest way to reproducably build the project. To do this you need to have Docker and Yarn installed on your system. Once these are installed, you can build the extension by running `yarn release`. The built extension files will be placed in the `dist` directory.

#### Building Natively

Building this extension natively requires Node.js 16.x and Yarn 1.x on your system. Once these are installed, you can build the extension by running `yarn && yarn types && yarn build`. The built extension files will be placed in the `dist` directory.
